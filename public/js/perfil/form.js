$('#optgroup').multiSelect({ selectableOptgroup: true });

validateName();

function validateName() {
    let method = $('#perfilForm').attr('method');
    if (method === 'PUT') {
        $('.fild-perfil').prop('readonly', true);
    }
}

$("#button-sendForm").on("click", function(){
    $('#perfilForm').submit(function (e) {
        e.preventDefault();
    });

    let url = $('#perfilForm').attr('action');
    let method = $('#perfilForm').attr('method');
    let dados = $('#perfilForm').serialize();    

    $.ajax({
        type: method,
        url: url,
        cache: false,
        data: dados,
        success: function (data) {

            let msg = '';

            if (data.success == true) {

                let msg = data.message;

                Swal.fire({
                    position: 'top-end',
                    width: 400,
                    type: 'success',
                    title: msg,
                    showConfirmButton: false,
                    timer: 2200
                });

                setTimeout(function () {

                    window.location.replace(window.__URL_BASE + "/admin/perfil");

                }, 2200);

            } else {
                let res = [];

                $.each(data.errors, function (index, value) {
                    res.push(value[0]);
                });

                msg = res.join('<br>');

                Swal.fire({
                    position: 'top-end',
                    type: 'error',
                    title: msg,
                    showConfirmButton: false,
                    timer: 3500
                });
            }
        }
    });

});