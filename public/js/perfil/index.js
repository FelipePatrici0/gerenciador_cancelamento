$(document).ready(function() {

    $('#table_list_perfis').DataTable({
        "ajax": {
            "url": window.__URL_BASE + "/admin/perfis",
            "dataType": "json",
        },

        "responsive": true,
        "processing": true,
        "serverSide": true,
        "language": {
            "url": window.__URL_BASE + '/plugins/dataTables/Portuguese-Brasil.json'
        },

        "order": [[0, 'desc']],
        "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
        "pageLength": 10,

        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "description"},
            
        ],
        "aoColumnDefs": [
            {
                "aTargets": [3],
                "bSortable": false,
                "mRender": function (data, type, full) {

                    let btnEditar = '<a href="'+ window.__URL_BASE+'/admin/perfil/'+full.id+'/edit"  class="btn btn-sm btn-clean btn-icon btn-icon-md" aria-expanded="false"><i class="la la-edit"></i></a>';

                    return btnEditar;
                    
                }
            },
        ]
    });

});