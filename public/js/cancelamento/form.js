$(document).ready(function (){

	validateFormPut();

	$("#button-consulta-cliente").click(function(e){
		e.preventDefault();
		$("#modalConsultaCliente").modal();
	})

	$("#data_solicitacao").datepicker({
	   language: 'pt-BR', 
	   autoclose: true,
	   orientation: 'bottom auto',
   });
	   
    $("#tipo_ligacao").select2();

    $("#status_cancelamento").select2({
        language: 'pt-BR',
        allowClear: true,
		width: "100%",
		ajax: {
			url: window.__URL_BASE + "/status-cancelamento",
			dataType: "json",
			delay: 250,
			data: function (params) {
				return {
					parametro: params.term
				};
			},

			processResults: function (data, params) {
				params.page = params.page || 1;
		
				return {
					results: data.data,
					pagination: {
						more: params.page * 30 < data.total
					}
				};
			},
			cache: true
		},
		placeholder: '...',
		escapeMarkup: function (markup) {
			return markup;
		},
		minimumInputLength: 0,
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
	}); 

	$("#produto").select2({
        language: 'pt-BR',
        allowClear: true,
        dataType: "json",
		width: "100%",
		ajax: {
			url: window.__URL_BASE + "/busca/produto",
			dataType: "json",
			delay: 250,
			data: function (params) {
				return {
					parametro: params.term
				};
			},

			processResults: function (data, params) {
				params.page = params.page || 1;
		
				return {
					results: data.data,
					pagination: {
						more: params.page * 30 < data.total
					}
				};
			},
			cache: true
		},
		placeholder: '...',
		escapeMarkup: function (markup) {
			return markup;
		},
		minimumInputLength: 0,
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
    }); 


    $("#motivo_cancelamento").select2({
        language: 'pt-BR',
        allowClear: true,
        dataType: "json",
		width: "100%",
		ajax: {
			url: window.__URL_BASE + "/motivo-cancelamento",
			dataType: "json",
			delay: 250,
			data: function (params) {
				return {
					parametro: params.term
				};
			},

			processResults: function (data, params) {
				params.page = params.page || 1;
		
				return {
					results: data.data,
					pagination: {
						more: params.page * 30 < data.total
					}
				};
			},
			cache: true
		},
		placeholder: '...',
		escapeMarkup: function (markup) {
			return markup;
		},
		minimumInputLength: 0,
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
    });  

    $("#recebimento_carta").select2({
        language: 'pt-BR',
        allowClear: true,
        dataType: "json",
		width: "100%",
		ajax: {
			url: window.__URL_BASE + "/carta-cancelamento",
			dataType: "json",
			delay: 250,
			data: function (params) {
				return {
					parametro: params.term
				};
			},

			processResults: function (data, params) {
				params.page = params.page || 1;
		
				return {
					results: data.data,
					pagination: {
						more: params.page * 30 < data.total
					}
				};
			},
			cache: true
		},
		placeholder: '...',
		escapeMarkup: function (markup) {
			return markup;
		},
		minimumInputLength: 0,
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
	});  

    function formatRepo(repo) {
		if (repo.loading) {
			return repo.text;
		}

		return repo.descricao;
	}

	function formatRepoSelection(repo) {
		return repo.descricao || repo.text;
	}

	$(".button-sendForm").click(function (){
		sendForm();
	});
	 
	function sendForm() {

		$(".data-form").submit(function (e) {
			e.preventDefault();
		});
		
		let url 	= $(".data-form").attr('action');
		let method 	= $(".data-form").attr('method');
		let data    = $(".data-form").serialize();

		$.ajax({
			url: url,
			type: method,
			cache: false,
			dataType: 'json',
			data: data,
			success: function (data){
				if(data.success == true){
					alert(data.message);

					$(location).attr('href', window.__URL_BASE + "/cancelamento")

				}else{
					let res = [];
					
					$.each(data.message, function (index, value){
						res.push("<li class='text-left'>" + value[0] + "</li>");
					});

					alert(res);
				}
			}
		});
	}
	
	function validateFormPut(){
		let method = $(".data-form").attr('method');
		
		if(method == 'PUT'){
			$("#div-status-cancelamento").hide();	
		}
	}


});