$(document).ready(function (){

    $("#status_cancelamento").select2({
        language: 'pt-BR',
		width: "100%",
		ajax: {
			url: window.__URL_BASE + "/status-cancelamento",
			dataType: "json",
			delay: 250,
			data: function (params) {
				return {
					parametro: params.term
				};
			},

			processResults: function (data, params) {
				params.page = params.page || 1;
		
				return {
					results: data.data,
					pagination: {
						more: params.page * 30 < data.total
					}
				};
			},
			cache: true
		},
		placeholder: 'Buscar Status Cancelamento...',
		escapeMarkup: function (markup) {
			return markup;
		},
		minimumInputLength: 0,
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
	}); 
	
    
    $("#motivo_cancelamento").select2({
        language: 'pt-BR',
		width: "100%",
		ajax: {
			url: window.__URL_BASE + "/motivo-cancelamento",
			dataType: "json",
			delay: 250,
			data: function (params) {
				return {
					parametro: params.term
				};
			},

			processResults: function (data, params) {
				params.page = params.page || 1;
		
				return {
					results: data.data,
					pagination: {
						more: params.page * 30 < data.total
					}
				};
			},
			cache: true
		},
		placeholder: 'Buscar Status Cancelamento...',
		escapeMarkup: function (markup) {
			return markup;
		},
		minimumInputLength: 0,
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
	});  

    $("#periodo_fitro").datepicker({
	 	language: 'pt-BR', 
		autoclose: true,
		orientation: 'bottom auto',
	});
	
	$("#btn-busca-cancelamento").click( function (e){
		e.preventDefault();
		callTable();
	});

	$("#btn-limpar-filtro").click( function(e){
		e.preventDefault();
		$('#status_cancelamento').val(null).trigger('change');
		$('#motivo_cancelamento').val(null).trigger('change');
		$('#dataInicio').val(null).trigger('change');
		$('#dataFim').val(null).trigger('change');
		
		callTable();
	}); 
 
	let tbCancelamento = $('#table_list_cancelamentos').DataTable({
		"ajax": {
			"url": window.__URL_BASE + "/cancelamento",
			"dataType": "json",
			"data": function (d) {
				d.status_cancelamento = $('#status_cancelamento').val();
				d.motivo_cancelamento = $('#motivo_cancelamento').val();
				d.dataInicio 		  = $('#dataInicio').val();
				d.dataFim 			  = $('#dataFim').val();
			},
			"dataSrc": function (json) {
				for (var i = 0; i < json.data.length; i++) {
					json.data[i].seq = i;
				}
				window._dataJSON = json;
				
				return json.data;
			} 
		},

		"language": {
			"url": window.__URL_BASE + '/plugins/dataTables/Portuguese-Brasil.json'
		},

		"responsive": false,
		"processing": true,
		"serverSide": true,
		"lengthMenu": [
			[10, 25, 50, 75, 100, 200, 300, 400, 500, 600],
			['10', '25', '50', '75', '100', '200', '300', ' 400', '500', '600']
		],
		"order": [[0, 'desc']],
		"columns": [
			{ "data": "id" },
			{ "data": "csid" },
			{ "data": "nome" },
			{ "data": "produto"},
			{ "data": "motivoCancelamento" },
			{ "data": "statusCancelamento" },	
			{ "data": "created_at"},
			{ "data": "data_solicitacao" },		
		],

		"aoColumnDefs": [
			{
				"aTargets": [6],
				"mRender": function (created_at, type, full) {
					if(created_at !== null){
						return moment(created_at).format("DD/MM/YYYY")
					}
					return ''
				}
			},
			{
				"aTargets": [7],
				"mRender": function (data_solicitacao, type, full) {
					
					if(data_solicitacao !== null){
						return moment(data_solicitacao).format("DD/MM/YYYY")
					}
					return ''
				}
			},
			{
				"aTargets": [8],
				"bSortable": false,
				"mRender": function (data, type, full) {

					var carta = '';
					var Os	  = '';

					if(full.arquivo_cancelamento.length > 0){
					
						for(var indice = 0; indice < full.arquivo_cancelamento.length; indice++){
							let item = full.arquivo_cancelamento[indice];

							if(item['tipo_arquivo'] == "Carta"){
								var carta = item['tipo_arquivo'] + "," + item['arquivo'] + "," + item['id'];

							}else{
								var Os = item['tipo_arquivo'] + "," + item['arquivo'] + "," + item['id'];
							}
						} 

					}  

					let btnArquivoCarta = full.id_status_cancelamento      == 1 && carta == '' ? `<a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalAnexarArquivosCarta" data-tipo_recebimento="${full.recebimentoCarta}" data-id_cancelamento="${full.id}"><i class="fa flaticon-attachment"></i> Anexar Carta</a>` : '';
					let btnArquivoOS    = full.id_status_cancelamento 	   == 5 && Os == '' ? `<a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalAnexarArquivosOS" data-tipo_recebimento="${full.recebimentoCarta}" data-id_cancelamento="${full.id}"><i class="fa flaticon-attachment"></i> Anexar OS</a>` : '';
			        let btnShowFile		= full.arquivo_cancelamento.length != 0 ? `<a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalVisualizarArquivos" data-id_cancelamento="${full.id}" data-carta="${carta}" data-os="${Os}"><i class="fa flaticon-folder-1"></i> Visualizar Arquivos</a>` : '';

					let menu = `
					<span class="dropdown">
						<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">
							<i class="la la-ellipsis-h"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="display: none; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1321px, 366px, 0px);">

							<a class="dropdown-item btn-detalhes" href="#" data-toggle="modal"  indexData="${full.seq}"><i class="la la-eye"></i> Detalhes</a>
							
							<a href="${window.__URL_BASE}/cancelamento/${full.id}/edit" class="dropdown-item" title="Editar"><i class="la la-edit"></i>Editar Registro</a>

							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalStatus" data-id_cancelamento="${full.id}" data-cliente="${full.nome}" data-status="${full.statusCancelamento}"><i class="fa flaticon-exclamation-1"></i> Editar Status</a>

							${btnArquivoCarta}

							${btnArquivoOS}

							${btnShowFile}

							<a class="dropdown-item" data-toggle="modal" data-target="#modalInteracao" href="#" data-id_cancelamento="${full.id}"><i class="fa flaticon-chat-1" ></i> Interações</a>
						</div>
					</span>
					`;
					
					return menu;  
				}
			},
		]

	});

    function formatRepo(repo) {
		if (repo.loading) {
			return repo.text;
		}

		return repo.descricao;
	}

	function formatRepoSelection(repo) {
		return repo.descricao || repo.text;
	} 

	function callTable(){
		tbCancelamento.ajax.reload(null, false);
	} 

	setInterval(function (){
		callTable();
	}, 60000);
 
});