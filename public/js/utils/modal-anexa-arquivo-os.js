
$("#modalAnexarArquivosOS").on('show.bs.modal', function (event){ 

    let button           = $(event.relatedTarget);
    let id_cancelamento  = button.data('id_cancelamento');

    $(".btn-send-file").unbind('click').click(function (e){
        e.preventDefault();
        sendFormOs(id_cancelamento);
    });

});

$("#modalAnexarArquivosOS").on('hide.bs.modal', function (event){ 

    $("#arquivoOs").val(null);
    $("#observacoesOs").val(null);

});

function sendFormOs(id_cancelamento){
    let data = new FormData($("#form-file-os")[0]);
    let tableListCancelamentos  = $("#table_list_cancelamentos").DataTable();

    $.ajax({
        url: window.__URL_BASE + "/cancelamento/arquivo/" + id_cancelamento,
        type: 'POST',
        enctype: 'multipart/form-data',
        cache: false,
        data: data,
        processData: false,
        contentType: false, 
        success: function (data){
            if(data.success == true){
                alert(data.message);

                tableListCancelamentos.ajax.reload(null, false);
                $('#modalAnexarArquivosOS').modal('hide');
                
            }else{
                let res = [];

                $.each(data.message, function (index, value){
                    res.push("<li class='text-left'>" + value[0] + "</li>");
                });

                alert(res);
            }
        }
    });
};
