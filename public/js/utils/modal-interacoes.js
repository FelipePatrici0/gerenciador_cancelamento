$('#modalInteracao').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget) // Button that triggered the modal
    let id_cancelamento = button.data('id_cancelamento') // Extract info from data-* attributes


    $("#tbInteracoes").dataTable().fnDestroy();
    $("#tbInteracoes").DataTable({
        "ajax": {
            "url": window.__URL_BASE + "/cancelamento/interacoes/list?id_cancelamento=" + id_cancelamento,
            "dataType": "json",
        },

        "responsive": true,
        "processing": true,
        "serverSide": true,
        "searching": false,

        "language": {
            "url": window.__URL_BASE + '/plugins/dataTables/Portuguese-Brasil.json'
        },

        "order": [[0, 'desc']],
        "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
        "pageLength": 5,
        "columns": [
            { "data": "id" },
            { "data": "usuario" },
            { "data": "status" },
            { "data": "observacoes" },
            { "data": "created_at" },
        ],
        "aoColumnDefs": [
            {
                "aTargets": [4],
                "bSortable": true,
                "mRender": function (datahorainicial, type, full) {
                  
                    return moment(datahorainicial, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY HH:mm:ss');

                }
            },
        ]
    });
})

