$(document).ready(function(){
    
    var tbCancelamentos = $("#table_list_cancelamentos").DataTable();
    tbCancelamentos.on('draw', function(){
        $(".btn-detalhes").click(function (e){
            e.preventDefault();

            let item = window._dataJSON.data[parseInt($(this).attr('indexData'))];

           $(".detalhes-cancelamento").html(
                (item.id                 != null         ?  '<p><strong>ID: </strong> '                   + item.id                                            + '</p>' : '') +
                (item.nome               != null         ?  '<p><strong>Nome: </strong> '                 + item.nome                                          + '</p>' : '') +
                (item.csid               != null         ?  '<p><strong>Csid: </strong> '                 + item.csid                                          + '</p>' : '') +
                (item.inicio_contrato    != null         ?  '<p><strong>Inicio do Contrato: </strong> '   + moment(item.inicio_contrato).format("DD/MM/YYYY")  + '</p>' : '') +
                (item.produto            != null         ?  '<p><strong>Produto: </strong> '              + item.produto                                       + '</p>' : '') +
                (item.created_at         != null         ?  '<p><strong>Data do contato: </strong> '      + moment(item.created_at).format("DD/MM/YYYY")       + '</p>' : '') +
                (item.statusCancelamento != null         ?  '<p><strong>Status Cancelamento: </strong> '  + item.statusCancelamento                            + '</p>' : '') +
                (item.motivoCancelamento != null         ?  '<p><strong>Motivo Cancelamento: </strong> '  + item.motivoCancelamento                            + '</p>' : '') +
                (item.placa_veiculo      != null         ?  '<p><strong>Placa do Veículo: </strong> '     + item.placa_veiculo                                 + '</p>' : '') +
                (item.data_solicitacao   != null         ?  '<p><strong>Data de Solicitação: </strong> '  + moment(item.data_solicitacao).format("DD/MM/YYYY") + '</p>' : '') +
                (item.contato            != null         ?  '<p><strong>Contato: </strong> '              + item.contato                                       + '</p>' : '') +
                (item.recebimentoCarta   != null         ?  '<p><strong>Recebimento da Carta: </strong> ' + item.recebimentoCarta                              + '</p>' : '') +
                (item.tipo_ligacao       != null         ?  '<p><strong>Tipo de Ligação: </strong> '      + item.tipo_ligacao                                  + '</p>' : '') +
                (item.observacoes        != null         ?  '<p><strong>Observaçoes: </strong> '          + item.observacoes                                   + '</p>' : '')
           );

           $("#modalDetalhes").modal('show'); 
        });
    });

});