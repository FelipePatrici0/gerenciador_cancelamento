
$("#modalStatus").on('show.bs.modal', function (event){ 

    let button          = $(event.relatedTarget);
    let id_cancelamento = button.data('id_cancelamento');
    let nome            = button.data('cliente');

    $(".modal-status").text(nome);
    sessionStorage.setItem("id_cancelamento", id_cancelamento)
});

$("#modalStatus").on('hide.bs.modal', function (event){ 

    $("#id_status_cancelamento").children('option').remove();
    $("#observacoes").val(null);

});

$("#id_status_cancelamento").select2({
    language: 'pt-BR',
    allowClear: true,
    width: "100%",
    ajax: {
        url: window.__URL_BASE + "/status-cancelamento", 
        dataType: "json",
        delay: 250,
        data: function (params) {
            return {
                parametro: params.term
            };
        },

        processResults: function (data, params) {
            params.page = params.page || 1;
    
            return {
                results: data.data,
                pagination: {
                    more: params.page * 30 < data.total
                }
            };
        },
        cache: true
    },
    placeholder: '...',
    escapeMarkup: function (markup) {
        return markup;
    },
    minimumInputLength: 0,
    templateResult: formatRepo,
    templateSelection: formatRepoSelection
}); 

function formatRepo(repo) {
    if (repo.loading) {
        return repo.text;
    }

    return repo.descricao;
}

function formatRepoSelection(repo) {
    return repo.descricao || repo.text;
}

$("#buttoSaveForm").click(function(e){
    e.preventDefault();

    let tableIndex  = $("#table_list_cancelamentos").DataTable();
    let data        = $("#StatusForm").serialize();

    $.ajax({
        url:  window.__URL_BASE + "/cancelamento/status/update/" + sessionStorage.getItem("id_cancelamento"),
        type: 'POST',
        cache: false,
        data: data,
        async:false,
        success: function (data){
            if(data.success == true){
                alert(data.message);

                tableIndex.ajax.reload(null, false);
                $('#modalStatus').modal('hide');

            }else{
                let res = [];

                $.each(data.message, function (index, value){
                    res.push("<li class='text-left'>" + value[0] + "</li>");
                });

                alert(res);
            }
        }
    });

});