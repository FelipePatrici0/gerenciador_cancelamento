sessionStorage.setItem("tipo_modo_query", '');

$("#modoConsultafortes").select2({
    width: "100%",
    allowClear: true,
    placeholder: 'Selecione...',
});

$("#modoConsultafortes").change(function(){
    let tipo_modo_query = $(this).val();
    sessionStorage.setItem("tipo_modo_query", tipo_modo_query);
    $("#parametroConsultafortes option").remove();
 
});

$("#parametroConsultafortes").select2({
    width: "100%",
    language: 'pt-BR',
    allowClear: true,
    ajax: {
        url: window.__URL_BASE + "/busca/cliente",
        dataType: "json",
        delay: 250,
        data: function (params) {
            return {
                parametro: params.term,
                modo_query: sessionStorage.getItem("tipo_modo_query"),
            };
        },
        processResults: function (data, params) {
            params.page = params.page || 1;

            return {
                results: data.data,
                pagination: {
                    more: params.page * 30 < data.total
                }
            };
        },
        cache: true

    },
    placeholder: 'Buscar Cliente...',
    escapeMarkup: function (markup) {
        return markup;
    },
    minimumInputLength: 3,
    templateResult: formatRepoDetails,
    templateSelection: formatRepoSelection
});

$("#parametroConsultafortes").on('select2:select', function (e){

    let data = e.params.data;
    
    $("#csid").val(data.id);
    $("#nome").val(data.Nome);
    $("#incioContrato").val(data.Aquisicao.replace(/(\d*)-(\d*)-(\d*).*/, '$3/$2/$1'));
    $("#nomeFantasia").val(data.NomeFantasia);
    $("#rg").val(data.RG);
    $("#cpfCnpj").val(data.CNPJCPF);
    $("#dddFone").val(data.DDD);
    $("#fone").val(data.Fone);
    $("#dddCelular").val(data.DDD_CELULAR);
    $("#celular").val(data.Celular);
    $("#email").val(data.Email);
    $("#cep").val(data.ENDCEP);
    $("#endereco").val(data.EndLogradouro);
    $("#numeroResidencia").val(data.EndNumero);
    $("#bairro").val(data.ENDBairro);
    $("#complemento").val(data.EndComplemento);
    $("#pontoReferencia").val(data.PontoRef);
    $("#estado").val(data.Estado);
    $("#cidade").val(data.Cidade); 
   
});


$("#modalConsultaCliente").on('hide.bs.modal', function (event){ 
    $("#parametroConsultafortes").children('option').remove();
    $('.inputInfoCadastroClean').val(null);

    $("#vencimentoDiversos").dataTable().fnDestroy();
    $("#vencimentoDiversos tbody").empty();
    
   // $("#vencimentoDiversos").dataTable().clear().draw();

});

$("#botaoImportar").click(function(e){
    e.preventDefault();

    let fildModalName       = $("#nome").val();
    let fildModalCsid       = $("#csid").val();
    let fildModalAquisicao  = $("#incioContrato").val();

    if(fildModalCsid.length > 0){
        $("#nameCliente").val(fildModalName);
        $("#csidCliente").val(fildModalCsid);
        $("#inicioControtoCliente").val(fildModalAquisicao);
        $("#modalConsultaCliente").modal('hide');

    }else{

      alert("Nenhum cliente foi selecionado...")
    }

});

$("#btnLoadtableInfoFinanceiras").on("click", function (e) {
    e.preventDefault();
    let csid = $("#csid").val();
    
    if(csid != ''){
        $("#vencimentoDiversos").dataTable().fnDestroy();
        $("#vencimentoDiversos").DataTable({
            "ajax": {
                "url":  window.__URL_BASE + "/busca/vencimento/diversos?csid=" + csid,
                "dataType":"json",
            },

            "processing": true,
            "serverSide": true,
            "searching": false,

            "language": {
                "url":  window.__URL_BASE +'/plugins/dataTables/Portuguese-Brasil.json'
            },

            "aaSorting": [[0, 'desc']],
            "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
            "pageLength": 5,
            "columns":[
                { "data": "CSID"},
                { "data": "NOME_CLI"},
                { "data": "VALOR_BOLETO" },
                { "data": "RENEGOCIACAO" },
                { "data": "DTVENC" },
                { "data": "DATA_PAGAMENTO" },
                { "data": "VALOR_PAGO" }
            ],
            "aoColumnDefs":[
                {
                    "aTargets": [ 2 ],
                    "bSortable":false,
                    "mRender": function ( VALOR_BOLETO, type, full )  {
                        let valor_boleto = parseFloat(VALOR_BOLETO);
                        return 'R$ ' + valor_boleto.toFixed(2);
                    }
                },
                {
                    "aTargets": [ 3 ],
                    "bSortable": false,
                    "mRender": function ( RENEGOCIACAO, type, full )  {
                        if(RENEGOCIACAO === 1){
                            return 'SIM';
                        }else{
                            return 'NÃO';
                        }
                    }
                },
                {
                    "aTargets": [ 4 ],
                    "bSortable":false,
                    "mRender": function ( DTVENC, type, full )  {
                        if(DTVENC !== null){
                            return moment(DTVENC, 'YYYY-MM-DD').format('DD/MM/YYYY');
                        }else{
                            return "";
                        }
                    }
                },
                {
                    "aTargets": [ 5 ],
                    "bSortable": false,
                    "mRender": function ( DATA_PAGAMENTO, type, full )  {
                        if(DATA_PAGAMENTO !== null){
                            return moment(DATA_PAGAMENTO, 'YYYY-MM-DD').format('DD/MM/YYYY');
                        }else{
                            return "";
                        }
                    }
                }, 
                {
                    "aTargets": [ 6 ],
                    "bSortable":false,
                    "mRender": function ( VALOR_PAGO, type, full )  {
                        if(VALOR_PAGO !== null){
                            let valor_pago = parseFloat(VALOR_PAGO);
                            return 'R$ ' + valor_pago.toFixed(2);
                        }else{
                            return "R$ 0.00";
                        }
                    }
                },
                {
                    "aTargets": [ 7 ],
                    "bSortable": false,
                    "mRender": function ( data, type, full )  {  
                        let date            = new Date();
                        let dataAtual       = moment(date).format('YYYY-MM-DD');
                        let dataVencimento  = full.DTVENC;
                        let valorPago       = full.VALOR_PAGO;

                        if(valorPago == null && dataAtual > dataVencimento){
                            return '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">' + 'Atrasado' + '</span>';

                        }else if(valorPago == null && dataAtual < dataVencimento){
                            return '<span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill">' + 'Aberto' + '</span>';

                        }else{
                            return '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">' + 'Pago' + '</span>';
                        } 
                    }
                }, 
            ]
        });

        $("#vencimentoCartao").dataTable().fnDestroy();
        $("#vencimentoCartao").DataTable({
            "ajax": {
                "url":  window.__URL_BASE + '/busca/vencimento/cartao?csid=' + csid,
                "dataType":"json"
            },
           
            "processing": true,
            "serverSide": true,
            "searching": false,

            "language": {
                "url":  window.__URL_BASE +'/plugins/dataTables/Portuguese-Brasil.json'
            },

            "aaSorting": [[1, 'asc']],
            "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
            "pageLength": 5,
            "columns": [
                { "data": "CSID" },
                { "data": "NOME_CLIENTE" },
                { "data": "VALOR_BOLETO" },
                { "data": "RENEGOCIACAO" },
                { "data": "DATA_VENCIMENTO" },
                { "data": "BVC.Data" },
                { "data": "VALOR_PAGO" },
            ],
            "aoColumnDefs":[
                {
                    "aTargets": [ 2 ],
                    "bSortable":false,
                    "mRender": function ( VALOR_BOLETO, type, full )  {
                        let valor_boleto = parseFloat(VALOR_BOLETO);
                        return 'R$ ' + valor_boleto.toFixed(2);
                    }
                },
                {
                    "aTargets": [ 3 ],
                    "bSortable": false,
                    "mRender": function ( RENEGOCIACAO, type, full )  {
                        if(RENEGOCIACAO === 1){
                            return 'SIM';
                        }else{
                            return 'NÃO';
                        }
                    }
                },
                {
                    "aTargets": [ 4 ],
                    "bSortable":false,
                    "mRender": function ( DATA_VENCIMENTO, type, full )  {
                        return moment(DATA_VENCIMENTO, 'YYYY-MM-DD').format('DD/MM/YYYY');
                        
                    }
                },
                {
                    "aTargets": [ 5 ],
                    "bSortable": true,
                    "mRender": function ( DATA_PAGAMENTO, type, full )  {
                        if(full.DATA_PAGAMENTO !== null){
                            return moment(full.DATA_PAGAMENTO, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
                        }
                            return "";
                    }
                },
                {
                    "aTargets": [ 6 ],
                    "bSortable":false,
                    "mRender": function ( VALOR_PAGO, type, full )  {
                        if(VALOR_PAGO !== null){
                            let valor_pago = parseFloat(VALOR_PAGO);
                            return 'R$ ' + valor_pago.toFixed(2);
                        }else{
                            return "R$ 0.00";
                        }
                    }
                },
                {
                    "aTargets": [ 7 ],
                    "bSortable": false,
                    "mRender": function ( data, type, full )  {  
                        let date            = new Date();
                        let dataAtual       = moment(date).format('YYYY-MM-DD');
                        let dataVencimento  = full.DATA_VENCIMENTO;
                        let valorPago       = full.VALOR_PAGO;

                        if(valorPago == null && dataAtual > dataVencimento){
                            return '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">' + 'Atrasado' + '</span>';

                        }else if(valorPago == null && dataAtual < dataVencimento){
                            return '<span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill">' + 'Aberto' + '</span>';

                        }else{
                            return '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">' + 'Pago' + '</span>';
                        } 
                    }
                }, 
            ],
        });

    }else{
        alert("Nenhum Cliente foi Selecionado...");
    } 
    
});



function formatRepoSelection(repo) {
    return repo.Nome || repo.text;
}

function formatRepoDetails(repo){
    if(repo.loading){
        return repo.text;
    }
     
    let data = `<p  style="color:#479FF8; font-size:14px;"> CSID: ${repo.id} - CNPJCPF: ${repo.CNPJCPF} </p>`;

    return repo.Nome + '</br>' + data;
} 

function formatRepo(repo) {
    if (repo.loading) {
        return repo.text;
    }

    return repo.Nome;
}
 