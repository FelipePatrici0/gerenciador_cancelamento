
$("#modalVisualizarArquivos").on('show.bs.modal', function (event){ 

  let button                  = $(event.relatedTarget);
  let arquivoCarta            = button.data('carta');
  let arquivoOs               = button.data('os');

  $("#showArchiveCarta").append('<div id="archiveCarta"></div>');
  $("#showArchiveOs").append('<div id="archiveOs"></div>');

  showArchive(arquivoCarta, arquivoOs);

});

$("#modalVisualizarArquivos").on('hide.bs.modal', function (event){ 
  $("#arquivo-carta").val(null);
  $(".aviso-arquivo").remove();
  $("#archiveCarta").remove();
  $("#archiveOs").remove();
  $("#uploadNewFileCarta").hide();
  $("#uploadNewFileOs").hide();
  $("#updateArquivoOs").val(null);
  $("#updateArquivoCarta").val(null);
  $(".btn-change-file").show();
});  


function showArchive(arquivoCarta, arquivoOs){
    if(arquivoCarta != ''){
        let directoryCarta = arquivoCarta.split(",");
        PDFObject.embed(window.__URL_BASE + "/arquivos/" + directoryCarta[0] + "/" + directoryCarta[1], "#archiveCarta" );
        sessionStorage.setItem("id_archive_carta", directoryCarta[2]);
    
    }else{
        $("#archiveCarta").append('<strong class="aviso-arquivo"> Nenhum arquivo anexado...</strong>');
        $("#change-btn-Carta").hide();
    }
    
    if(arquivoOs != ''){
        let directoryOs = arquivoOs.split(",");
        PDFObject.embed(window.__URL_BASE + "/arquivos/" + directoryOs[0] + "/" + directoryOs[1],   "#archiveOs" );
        sessionStorage.setItem("id_archive_os", directoryOs[2]);
    
    }else{
        $("#archiveOs").append('<strong class="aviso-arquivo"> Nenhum arquivo anexado...</strong>');
        $("#change-btn-Os").hide();

    }
} 

interactionFormUpdateArchive();

function interactionFormUpdateArchive(){
  $(".btn-change-file").click(function(){
    let navLinkCarta = $("#carta").attr('aria-selected');
    let navLinkOs    = $("#os").attr('aria-selected');

    if(navLinkCarta === 'true' && navLinkOs === 'false'){
      $("#archiveCarta").hide();
      $("#uploadNewFileCarta").show();

    }else{
      $("#archiveOs").hide();
      $("#uploadNewFileOs").show();
    }

  });
}

interactionBtnCancel();

function interactionBtnCancel(){
  $(".btn-cancel-change-file").click(function(){
    let navLinkCarta = $("#carta").attr('aria-selected');
    let navLinkOs    = $("#os").attr('aria-selected');

    if(navLinkCarta === 'true' && navLinkOs === 'false'){
      $("#uploadNewFileCarta").hide();
      $("#archiveCarta").show();

    }else{
      $("#uploadNewFileOs").hide();
      $("#archiveOs").show();
    }
    
  });
}

$(".btn-send-file-update").click(function(e){
    e.preventDefault();
    let buttonName = $(this).attr('name');
    SendFormUpdateArchiveOS(buttonName);
  });

function SendFormUpdateArchiveOS(buttonName){
	let tbListCancelamentos  = $("#table_list_cancelamentos").DataTable();	

	if(buttonName == 'bntSendCarta'){
		var formArchive = new FormData($("#FomrArchiveCarta")[0]);
        var id_archive = sessionStorage.getItem("id_archive_carta");

	}else if(buttonName == 'btnSendOs'){
		var formArchive = new FormData($("#FomrArchiveOs")[0]);
        var id_archive = sessionStorage.getItem("id_archive_os");

	}else{
		var formArchive = '';
  }

  	if(formArchive != ''){
        $.ajax({
            url: window.__URL_BASE + "/cancelamento/arquivo/update/" + id_archive,
            type: 'POST',
            enctype: 'multipart/form-data',
            cache: false,
            data: formArchive,
            processData: false,
            contentType: false, 
            success: function (data){
                if(data.success == true){
                    alert(data.message);

                    tbListCancelamentos.ajax.reload(null, false);
                    $('#modalVisualizarArquivos').modal('hide');
                    
                }else{
                    let res = [];

                    $.each(data.message, function (index, value){
                    res.push("<li class='text-left'>" + value[0] + "</li>");
                    });

                    alert(res);
                }
            }
        });  
  	}else{
	  alert('Falha ao enviar as informações!');
	} 
    
}