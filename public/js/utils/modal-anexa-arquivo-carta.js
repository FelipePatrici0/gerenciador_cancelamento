
$("#modalAnexarArquivosCarta").on('show.bs.modal', function (event){ 

    let button           = $(event.relatedTarget);
    let id_cancelamento  = button.data('id_cancelamento');

    $(".btn-send-file").unbind('click').click(function (e){
        e.preventDefault();
        sendFormCarta(id_cancelamento);
    });

    fieldsInteractions();
});

$("#modalAnexarArquivosCarta").on('hide.bs.modal', function (event){ 

    $("#id_recebimento_carta").children('option').remove();
    $("#arquivo-carta").val(null);
    $("#contato").val(null);
    $("#observacoesCarta").val(null);

});

$("#id_recebimento_carta").select2({
    language: 'pt-BR',
    allowClear: true,
    width: "100%",
    ajax: {
        url: window.__URL_BASE + "/carta-cancelamento",
        dataType: "json",
        delay: 250,
        data: function (params) {
            return {
                parametro: params.term
            };
        },

        processResults: function (data, params) {
            params.page = params.page || 1;
    
            return {
                results: data.data,
                pagination: {
                    more: params.page * 30 < data.total
                }
            };
        },
        cache: true
    },
    placeholder: 'Buscar tipo de recebimento da carta...',
    escapeMarkup: function (markup) {
        return markup;
    },
    minimumInputLength: 0,
    templateResult: formatRepo,
    templateSelection: formatRepoSelection
}); 

function sendFormCarta(id_cancelamento){

    let tableListCancelamentos  = $("#table_list_cancelamentos").DataTable();
    let data = new FormData($("#form-file-carta")[0]);

    $.ajax({
        url: window.__URL_BASE + "/cancelamento/arquivo/" + id_cancelamento,
        type: 'POST',
        enctype: 'multipart/form-data',
        cache: false,
        data: data,
        processData: false,
        contentType: false, 
        success: function (data){
            if(data.success == true){
                alert(data.message);

                tableListCancelamentos.ajax.reload(null, false);
                $('#modalAnexarArquivosCarta').modal('hide');
                
            }else{
                let res = [];

                $.each(data.message, function (index, value){
                    res.push("<li class='text-left'>" + value[0] + "</li>");
                });

                alert(res);
            }
        }
    });
};

function formatRepo(repo) {
    if (repo.loading) {
        return repo.text;
    }

    return repo.descricao;
}

function formatRepoSelection(repo) {
    return repo.descricao || repo.text;
}

function fieldsInteractions(){
    $(".fildEmail").hide(function (){
        $("#id_recebimento_carta").change(function (){
            if($("#id_recebimento_carta").val() == 3){
                $(".fildEmail").show();
                $("#emailContato").val(null);
                $("#emailContato").prop("disabled", false);

            }else{
                $(".fildEmail").hide();
                $("#emailContato").prop("disabled", true);
            }
        });

    })

    $(".fildNumero").hide(function (){
        $("#id_recebimento_carta").change(function (){
            if($("#id_recebimento_carta").val() == 1){
                $(".fildNumero").show();
                $("#numeroContato").val(null);
                $("#numeroContato").prop("disabled", false);

            }else{
                $(".fildNumero").hide();
                $("#numeroContato").prop("disabled", true);
            }
        });
    })
}