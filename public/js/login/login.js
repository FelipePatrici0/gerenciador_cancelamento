$(document).ready(function() {

    $('#button_login').on("click",function(){
       
        $("#form_login").submit(function (e) {
            e.preventDefault();
		});
       
        $.ajax({
            type:'POST',
            url: window.__URL_BASE + '/login',
            dataType:'json',
            data: $('#form_login').serialize(),
            success: function(data){
                if(data.success === true){
                    window.location.replace(window.__URL_BASE + "/home");
                    
                }else{

                    let msg = data.message;
                    
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "3000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
                       
                    toastr.error(msg, "Error.");
                   
                }
            }
        }); 
    });

})