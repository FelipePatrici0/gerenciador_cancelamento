<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            @yield('subheader')
        </div>
    </div>
</div>
<!-- begin:: Content -->