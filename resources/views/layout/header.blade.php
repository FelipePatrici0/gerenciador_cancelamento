<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
    <div class="kt-header__top">
        <div class="kt-container ">
            <!-- begin:: Brand -->
            <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                <div class="kt-header__brand-logo">
                    <a href="index.html">
                        <img alt="Logo" src="{{asset('assets/media/logos/logo-locktec.png')}}" class="kt-header__brand-logo-default" />
                    </a>
                </div>
            </div>
            <!-- begin:: Header Topbar -->
            <div class="kt-header__topbar kt-grid__item kt-grid__item--fluid">
                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                        <span class="kt-header__topbar-welcome">Oi,</span>
                        <span class="kt-header__topbar-username">  {{ Auth::user()->name }} </span>
                        <img class="kt-hidden-" alt="Pic" src="{{asset('assets/media/users/default.jpg')}}"/>
                    </div>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                        <!--begin: Navigation -->
                        <div class="kt-notification">
                            <div class="kt-notification__custom kt-space-between">
                               <a href="{{route('logout')}}" class="btn btn-label btn-label-brand btn-sm btn-bold">Sair</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layout.headerMenu')
</div>

<!-- end:: Header -->