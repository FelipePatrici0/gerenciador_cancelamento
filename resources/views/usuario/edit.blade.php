@extends('layout.base')

@section('title', 'Listar')
    
@section('subheader')
    
@parent
    <h3 class="kt-subheader__title">Editar Usuario</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Editar</span>
@stop

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Novo Cadastro
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
            <a href="{{route('usuario.index')}}" class="btn btn-clean btn-icon-sm">
                    <i class="la la-long-arrow-left"></i>
                    Voltar
                </a>
                <div class="btn-group">
                    <button type="button" class="btn btn-info btn-icon-sm button-sendForm">
                        <i class="la la-save"></i> Salvar
                    </button>
                </div>
            </div>
        </div>
        <form class="data-form" action="{{route('cancelamento.update')}}" method="PUT">
            @include('usuario.form')
        </form>
    </div>
</div>
@endsection