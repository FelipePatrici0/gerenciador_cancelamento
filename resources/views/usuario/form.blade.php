<input type="hidden" class="_token" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
<div class="kt-portlet__body">
    <div class="form-group row">
        <div class="col-lg-5">
            <label>Nome</label>
            <input type="text" class="form-control" id="nameCliente" name="nome"  value="" placeholder="Nome...">
        </div>
        <div class="col-lg-5">
            <label>Sobrenome</label>
            <input type="text" class="form-control" id="nameCliente" name="nome"  value="" placeholder="Nome...">
        </div>
        <div class="col-lg-2">
            <label>Status:</label>
            <select class="form-control kt-input" name="status" id="status">
                <option value="A">Ativo</option>
                <option value="I">Inativo</option>
            </select>
        </div>
    </div>   
    <div class="form-group row">
        <div class="col-lg-4">
            <label>E-mail</label>
        <input type="email" class="form-control" name="email"  id="email" value="">
        </div>
        <div class="col-lg-4">
            <label>Senha</label>
        <input type="password" class="form-control" name="password" id="password" value="">
        </div>
        <div class="col-lg-4">
            <label>Empresa</label>
            <select class="form-control kt-input" name="empresa" id="tipoe_empresa">
                <option value="A">Locktec Tecnologia</option>
            </select>
        </div>
    </div>   
    {{-- <div class="form-group row">
        <div class="col-md-6 col-lg-4" >
            <select id='optgroup' multiple='multiple' name="perfis[]">
                <optgroup label='Perfis'>
                    @foreach($perfis as $perfil) 
                    <option value="{{$perfil->name}}" >{{$perfil->name}}</option>
                    @endforeach
                    </optgroup>
                    <optgroup label='Atuais perfis'>
                    @foreach($perfisAtuais as $perfilAtual)
                    <option value="{{$perfilAtual->name}}" {{'selected="selected"'}}>{{$perfilAtual->name}}</option>
                    @endforeach 
                </optgroup>
            </select>
        </div>
    </div>     --}}
</div>
@section('js')
    <script src="{{asset('js/usuario/form.js')}}"></script>
@endsection