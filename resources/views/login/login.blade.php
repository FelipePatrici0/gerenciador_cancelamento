@extends('login.base_login')

@section('title', 'Login')
    
@section('content_login')
<div class="kt-login__signin">
    <div class="kt-login__head">
        <h3 class="kt-login__title">Sistema de Cancelamento</h3>
    </div>
    <form class="kt-form" id="form_login">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="input-group">
            <input class="form-control" type="email" required placeholder="Email" name="email" value="felipe@locktec.com.br">
        </div>
        <div class="input-group">
            <input class="form-control" type="password" required placeholder="senha" name="password" autocomplete="off" value="123456">
        </div>
        <div class="row kt-login__extra">
            <div class="col">
                <label class="kt-checkbox">
                    <input type="checkbox" name="remember"> Lembrar-me
                    <span></span>
                </label>
            </div>
            <div class="col kt-align-right">
                <a href="javascript:;" id="kt_login_forgot" class="kt-login__link">Esqueceu a senha?</a>
            </div>
        </div>
        <div class="kt-login__actions">
            <button id="button_login" class="btn btn-secondary btn-pill kt-login__btn-secondary">Acessar</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script src="{{asset('js/login/login.js')}}"></script>
    
@endsection