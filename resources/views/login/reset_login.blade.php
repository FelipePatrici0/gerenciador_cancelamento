@extends('login.base_login')

@section('title','Reset Login')

@section('content_login_reset')
<div class="kt-login__forgot">
    <div class="kt-login__head">
        <h3 class="kt-login__title">Esqueceu sua senha ?</h3>
        <div class="kt-login__desc">Insira seu e-mail para redefinir sua senha:</div>
    </div>
    <form class="kt-form">
        <div class="input-group">
            <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
        </div>
        <div class="kt-login__actions">
            <button id="kt_login_forgot_submit" class="btn btn-secondary btn-pill kt-login__btn-secondary">Solicitar</button>&nbsp;&nbsp;
            <button id="kt_login_forgot_cancel" class="btn btn-secondary btn-pill kt-login__btn-secondary">Cancelar</button>
        </div>
    </form>
</div>
@endsection
