<input type="hidden" class="_token" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
<div class="kt-portlet__body">
    <div class="form-group row">
        <div class="col-lg-8">
            <label>Nome</label>
        <input type="text" class="form-control fild-perfil" id="namePerfil" name="nome"  value="{{$perfil->name}}" placeholder="Nome...">
        </div>
    </div> 
    <div class="form-group row">
        <div class="col-lg-8">
            <label>Descrição</label>
        <textarea class="form-control fild-perfil" id="descricao" name="descricao" rows="4" >{{$perfil->description}}</textarea>
        </div>
    </div> 
    <div class="form-group row">
        <div class="col-md-6 col-lg-8" id="select">
            <label for="">Seleção de permissões</label>
            <select id='optgroup' multiple='multiple' name="permissoes[]">
                <optgroup label='Permissões'>
                   @foreach($permissoes as $permissao) 
                    <option value="{{$permissao->name}}" >{{$permissao->name}}</option>
                    @endforeach
                </optgroup>
                <optgroup label='Atuais permissões'>
                    @foreach($permissoesPerfil as $permissaoPerfil)
                    <option value="{{$permissaoPerfil->name}}" {{'selected="selected"'}}>{{$permissaoPerfil->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>   
</div>
@section('js')
   
@endsection

@section('js')
    <script src="{{asset('js/perfil/form.js')}}"></script> 
@endsection