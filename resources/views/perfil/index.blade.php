@extends('layout.base')

@section('title', 'Perfis')
    
@section('subheader')
    
@parent
    <h3 class="kt-subheader__title">Perfis e Permissões</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Listagem</span>
@stop

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Perfis Cadastrados
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                    <a href="{{route('perfil.create')}}" class="btn btn-info btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            Novo
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="table-responsive" > 	
                    <div class="col-sm-12">
                        <table class="table table-striped table-bordered table-hover" id="table_list_perfis" >
                            <thead>
                                <tr role="row">
                                    <th style="width:10%" >ID</th>
                                    <th style="width:20%"> Nome </th>
                                    <th style="width:60%"> Descrição </th>
                                    <th style="width:10%">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script src="{{asset('js/perfil/index.js')}}"></script> 
@endsection