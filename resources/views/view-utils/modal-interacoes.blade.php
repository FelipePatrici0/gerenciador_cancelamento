<div class="modal fade" id="modalInteracao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Interações</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="tbInteracoes" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> id </th>
                                <th> Usuário </th>
                                <th> Status </th>
                                <th> Observação</th>
                                <th> Data / Hora</th>
                            </tr>
                        </thead>       
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>