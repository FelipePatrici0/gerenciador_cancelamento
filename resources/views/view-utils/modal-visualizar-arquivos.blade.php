
<div class="modal fade" id="modalVisualizarArquivos" role="dialog" aria-labelledby="exampleModalLabel" style="height: !important500px;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Visualizar Arquivos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="kt-portlet">
                    <div class="kt-portlet__body">
                        <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-primary" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="carta" data-toggle="tab" href="#kt_tabs_3_1" role="tab" aria-selected="true">Carta</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" id="os" href="#kt_tabs_3_2" role="tab" aria-selected="false">Ordem de Serviço</a>
                            </li>
                        </ul>                        
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_3_1" role="tabpanel">
                                <div id="showArchiveCarta"></div>
                                <div id="uploadNewFileCarta" style="display: none;">
                                    <form id="FomrArchiveCarta" class="kt-form kt-form--label-right">
                                        <input type="hidden" class="_token" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                                        <div class="kt-portlet__body">
                                            <div class="form-group row">
                                                <div class="col-md-6 col-lg-12">
                                                    <label>Selecione o arquivo</label>
                                                    <input type="file" class="form-control-file" name="arquivo" id="updateArquivoCarta" accept="application/pdf/jpeg/png" required>
                                                    <input type="hidden" name="tipo_arquivo" value="Carta">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__foot">
                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-lg-12 kt-align-right">
                                                        <button type="button" class="btn btn-brand btn-send-file-update" name="bntSendCarta">Enviar</button>
                                                        <button type="button" class="btn btn-secondary btn-cancel-change-file">Cancelar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <br>
                                <div class="form-group row">
                                    <div class="col-md-6 col-lg-6" id="div-btn-change-file-carta">
                                        <button class="btn btn-sm btn-clean btn-bold btn-upper btn-change-file" id="change-btn-Carta">Alterar arquivo</button>
                                    </div>
                                </div> 
                            </div>
                            <div class="tab-pane" id="kt_tabs_3_2" role="tabpanel">
                                <div id="showArchiveOs"></div>
                                <div id="uploadNewFileOs" style="display: none;">
                                    <form id="FomrArchiveOs" class="kt-form kt-form--label-right">
                                        <input type="hidden" class="_token" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                                        <div class="kt-portlet__body">
                                            <div class="form-group row">
                                                <div class="col-md-6 col-lg-12">
                                                    <label>Selecione o arquivo</label>
                                                    <input type="file" class="form-control-file" name="arquivo" id="updateArquivoOs" accept="application/pdf/jpeg/png" required>
                                                    <input type="hidden" name="tipo_arquivo" value="Os" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__foot">
                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-lg-12 kt-align-right">
                                                        <button type="button" class="btn btn-brand btn-send-file-update" name="btnSendOs">Enviar</button>
                                                        <button type="button" class="btn btn-secondary btn-cancel-change-file">Cancelar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form> 
                                </div>
                                <br>
                                <div class="form-group row">
                                    <div class="col-md-6 col-lg-6" id="div-btn-change-file-os">
                                        <button class="btn btn-sm btn-clean btn-bold btn-upper btn-change-file" id="change-btn-Os">Alterar arquivo</button>
                                    </div>
                                </div>   
                            </div>
                        </div>                             
                    </div>
                </div>
                <div class="modal-footer btnAlterarFechar">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>
