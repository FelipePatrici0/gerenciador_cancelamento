<!-- Modal status OS -->
<style>
    .select2-choices {
  min-height: 150px;
  max-height: 150px;
  overflow-y: auto;
}
</style>
<div class="modal fade" id="modalStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title modal-status" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="StatusForm">
                <div class="form-group">
                    <input type="hidden" class="_token" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
                </div>
                <div class="col-md-6 col-lg-12">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Status</label>
                        <select class="form-control" style="width: 100%;" id="id_status_cancelamento" name="id_status_cancelamento">
                            <option></option>
                        </select>	
                    </div>
                </div>
                <div class="col-md-6 col-lg-12">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Observações:</label>
                        <textarea class="form-control" id="observacoes" name="observacoes" rows="3"></textarea>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="button" id="buttoSaveForm" class="btn btn-brand btn-icon-sm">Salvar</button>
        </div>
        </div>
    </div>
</div>

