 <div class="modal fade" id="modalAnexarArquivosCarta" role="dialog" aria-labelledby="exampleModalLabel" style="height: !importante500px;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Anexar Carta de Cancelamento / Venda</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">       
                <form id="form-file-carta" class="kt-form kt-form--label-right" enctype="multipart/form-data">
                    <input type="hidden" class="_token" name="_token"  value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="tipo_arquivo" value="Carta">
                    <div class="col-md-6 col-lg-12">
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Meio de Recebimento:</label>
                            <select class="form-control" style="width: 100%;" id="id_recebimento_carta" name="id_recebimento_carta">
                                <option value=""></option>
                            </select>	
                        </div>
                        <div class="form-group row fildEmail">
                            <div class="col-md-6 col-lg-12">
                                <label>Email de Contato:</label>
                                <input type="email" class="form-control" name="contato" id="emailContato">
                            </div>
                        </div>
                        <div class="form-group row fildNumero">
                            <div class="col-md-6 col-lg-12">
                                <label>Número de Contato:</label>
                                <input type="text" class="form-control" name="contato" id="numeroContato">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 col-lg-12">
                                <label>Selecione o arquivo:</label>
                                <input type="file" class="form-control-file" name="arquivo" id="arquivo-carta" accept="application/pdf/jpeg/png" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 col-lg-12">
                                <label for="message-text" class="col-form-label">Observações:</label>
                                <textarea class="form-control" id="observacoesCarta" name="observacoes" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand btn-send-file" >Salvar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
