<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalConsultaCliente">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">
                    <span class="kt-portlet__head-icon">
                        <i class="fa flaticon2-search"></i>                                       
                    </span>  
                    Consulta de Cliente
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="kt-section__content">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">Consultar Por: </label>
                        <div class="col-10">
                            <select class="form-control"  id="modoConsultafortes" name="modo">
                                <option></option>
                                <option value="Nome">Nome</option>
                                <option value="Codigo">CSID</option>  
                                <option value="CNPJCPF">CPF / CNPJ</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">Cliente:</label>
                        <div class="col-10">
                            <select class="form-control kt-input" name="parametro" id="parametroConsultafortes">
                                <option></option>
                            </select>    
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label"></label>
                        <div class="col-5">
                            <button class="btn btn-info btn-elevate" id="botaoImportar">
                                <span>
                                    <i class="fa flaticon-arrows"></i>
                                    <span>Importar</span>
                                </span>
                            </button>
                        </div>
                    </div>  
                    <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-primary" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#div-fortes" role="tab" aria-selected="true">Fortes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#div-CRL" role="tab" aria-selected="false">CRL</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#div-rastreamento" role="tab" aria-selected="false">Rastreamento</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="div-fortes" role="tabpanel">
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                           Informações Cadastrais.
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                            <label>CSID:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly  id="csid">
                                        </div>
                                        <div class="col-lg-8">
                                            <label class="">Nome:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly  id="nome">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="">Inicio do contrato:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly id="incioContrato">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Nome Fantasia:</label>
                                            <input type="email" class="form-control inputInfoCadastroClean" Readonly id="nomeFantasia">
                                        </div>
                                        <div class="col-lg-3">
                                            <label>RG:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly id="rg">
                                        </div>
                                        <div class="col-lg-3">
                                            <label>CPF/CNPJ:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="cpfCnpj">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-1">
                                            <label>DDD:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="dddFone">
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Fone:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="fone">
                                        </div>
                                        <div class="col-lg-1">
                                            <label>DDD:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="dddCelular">
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Celular:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="celular">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Email:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="email">
                                        </div>
                                    </div> 
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <label>CEP:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="cep">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Endereço:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="endereco">
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Número:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="numeroResidencia">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <label>Bairro:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="bairro">
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Complemento:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly id="complemento">
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Ponto de Referência:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="pontoReferencia">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <label>Estado:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="estado">
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Cidade:</label>
                                            <input type="text" class="form-control inputInfoCadastroClean" Readonly value="" id="cidade">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                           Informações Financeiras.
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-label">
                                        <button class="btn btn-outline-brand btn-elevate btn-pill" id="btnLoadtableInfoFinanceiras">Carregar</button>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head kt-portlet__head--right kt-portlet__head--noborder  kt-ribbon kt-ribbon--clip kt-ribbon--left kt-ribbon--info">
                                            <div class="kt-ribbon__target" style="top: 12px;">
                                                <span class="kt-ribbon__inner"></span> Diversos
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fit-top">
                                            <div class="form-group row">
                                                <div class="table-responsive"> 	
                                                    <div class="col-sm-12">
                                                        <table class="table table-striped table-bordered table-hover" id="vencimentoDiversos" >
                                                            <thead>
                                                                <tr>
                                                                    <th>CSID</th>
                                                                    <th>Nome</th>
                                                                    <th>Valor Boleto</th>
                                                                    <th>Renegociação</th>
                                                                    <th>Vencimento</th>
                                                                    <th>Data Pagamento</th>
                                                                    <th>Valor Pago</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head kt-portlet__head--right kt-portlet__head--noborder  kt-ribbon kt-ribbon--clip kt-ribbon--left kt-ribbon--info">
                                            <div class="kt-ribbon__target" style="top: 12px;">
                                                <span class="kt-ribbon__inner"></span>Cartão
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fit-top">
                                            <div class="form-group row">
                                                <div class="table-responsive"> 	
                                                    <div class="col-sm-12">
                                                        <table class="table table-striped table-bordered table-hover" id="vencimentoCartao" >
                                                            <thead>
                                                                <tr>
                                                                    <th>CSID</th>
                                                                    <th>Nome</th>
                                                                    <th>Valor Boleto</th>
                                                                    <th>Renegociação</th>
                                                                    <th>Vencimento</th>
                                                                    <th>Data Pagamento</th>
                                                                    <th>Valor Pago</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane " id="div-CRL" role="tabpanel">
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                           Informações de Atendimento.
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-label">
                                        <button class="btn btn-outline-brand btn-elevate btn-pill">Carregar</button>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    CRL
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane " id="div-rastreamento" role="tabpanel">
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                           Informações de Rastreamento.
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-label">
                                        <button class="btn btn-outline-brand btn-elevate btn-pill">Carregar</button>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    Rastreamento
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>