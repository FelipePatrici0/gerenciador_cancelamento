<!-- Modal Detalhes OS -->
<div class="modal fade" id="modalDetalhes" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="exampleModalScrollableTitle">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-checking"></i>
                </span>
                Detalhes do cancelamento
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">	
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="kt-form__section kt-form__section--first">
                    <div class="kt-wizard-v3__review">
                        <div class="kt-wizard-v3__review-item">
                            <div class="content detalhes-cancelamento">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button target="_blank" data-dismiss="modal" class="btn btn-brand btn-icon-sm">Fechar</button>	
            </div>
        </div>
    </div>
</div>