<div class="modal fade" id="modalAnexarArquivosOS" role="dialog" aria-labelledby="exampleModalLabel" style="height: !importante500px;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Anexar Ordem de Serviço</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">       
                <form id="form-file-os" class="kt-form kt-form--label-right">
                    <input type="hidden" class="_token" name="_token"  value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="tipo_arquivo" value="Os" >
                    <div class="col-md-6 col-lg-12">
                        <div class="form-group row">
                            <div class="col-md-6 col-lg-12">
                                <label>Selecione o arquivo:</label>
                                <input type="file" class="form-control-file" name="arquivo" id="arquivoOs" accept="application/pdf/jpeg/png" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 col-lg-12">
                                <label for="message-text" class="col-form-label">Observações:</label>
                                <textarea class="form-control" id="observacoesOs" name="observacoes" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand btn-send-file" >Salvar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
