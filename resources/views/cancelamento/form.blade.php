<input type="hidden" class="_token" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
<div class="kt-portlet__body">
    <div class="form-group row">
        <div class="col-lg-6">
            <label>Nome do Cliente:</label>
            <input type="text" class="form-control" id="nameCliente" name="nome"  readonly value="{{$cancelamento->nome}}" placeholder="Nome...">
        </div>
        <div class="col-lg-2">
            <label>CSID:</label>
            <input type="text"class="form-control" id="csidCliente" name="csid" readonly value="{{$cancelamento->csid}}" placeholder="CSID...">
        </div>
        <div class="kt-section__content kt-section__content--solid top-button">
            <button type="button" class="btn btn-info" id="button-consulta-cliente">
                <i class="fa fa-search"></i>
                Consultar
            </button>
        </div>
    </div>   
    <div class="form-group row">
        <div class="col-lg-4">
            <label>Inicio do Contrato</label>
        <input type="text" class="form-control" name="inicio_contrato"  readonly id="inicioControtoCliente" value="{{$cancelamento->inicio_contrato}}">
        </div>
        <div class="col-lg-4">
            <label>Data de Solicitação</label>
        <input type="text" class="form-control" name="data_solicitacao" id="data_solicitacao" value="{{$cancelamento->data_solicitacao}}">
        </div>
        <div class="col-lg-4">
            <label>Placa do Veículo</label>
        <input type="text" class="form-control" name="placa_veiculo" placeholder="Placa..." value="{{$cancelamento->placa_veiculo}}">
        </div>
    </div>    
    <div class="form-group row">
        <div class="col-lg-4">
            <label>Tipo de Ligação:</label>
            <select class="form-control kt-input" name="tipo_ligacao" id="tipo_ligacao">
                <option value="Receptiva" {{$cancelamento->tipo_ligacao == 'Receptiva' ? 'selected="selected"' : ''}}>Receptiva</option>
                <option value="Ativa" {{$cancelamento->tipo_ligacao == 'Ativa' ? 'selected="selected"' : ''}}>Ativa</option>
            </select>
        </div> 
        <div class="col-lg-4">
            <label>Produto:</label>
            <select class="form-control kt-input" name="id_produto" id="produto">
            <option value="{{$produto->id}}" {{'selected="selected"'}}>{{$produto->descricao}}</option>
            </select>
        </div>
        <div class="col-lg-4">
            <label>Motivo Cancelamento:</label>
            <select class="form-control kt-input" name="id_motivo_cancelamento" id="motivo_cancelamento">
            <option value="{{$motivoCancelamento->id}}" {{'selected="selected"'}}>{{$motivoCancelamento->descricao}}</option>
            </select>
        </div>
    </div>  
    <div class="form-group row" id="div-status-cancelamento">
        <div class="col-lg-4">
            <label>Status Cancelamento:</label>
            <select class="form-control kt-input" name="id_status_cancelamento" id="status_cancelamento">
                <option value="{{$statusCancelamento->id}}" {{'selected="selected"'}}>{{$statusCancelamento->descricao}}</option>
            </select>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="exampleTextarea">Observações</label>
    <textarea class="form-control" id="exampleTextarea" name="observacoes" rows="6">{{$cancelamento->observacoes}}</textarea>
    </div>     
</div>

@include('view-utils.modal-busca-cliente')

@section('js')
    <script src="{{asset('js/cancelamento/form.js')}}"></script>
    <script src="{{asset('js/utils/modal-busca-cliente.js')}}"></script>
@endsection