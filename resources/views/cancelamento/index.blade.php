@extends('layout.base')

@section('title', 'Listar')
    
@section('subheader')
    
@parent
    <h3 class="kt-subheader__title">Cancelamento</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Listagem</span>
@stop

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Ultimos Registros
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> Export
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>                         
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        &nbsp;
                    <a href="{{route('cancelamento.create')}}" class="btn btn-info btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            Novo
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin::Form-->
            <form class="kt-form kt-form--label-right">
                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label>Status Cancelamento:</label>
                            <select class="form-control kt-input" name="status_cancelamento" id="status_cancelamento">
                                <option></option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Motivo Cancelamento:</label>
                            <select class="form-control kt-input" name="motivo_cancelamento" id="motivo_cancelamento">
                              <option></option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label class="">Periodo:</label>
                            <div class="input-daterange input-group" id="periodo_fitro">
                                <input type="text" class="form-control kt-input" name="dataInicio" placeholder="De" id="dataInicio"/>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa flaticon2-calendar-9"></i></span>
                                </div>
                                <input type="text" class="form-control kt-input" name="dataInicio" placeholder="Até" id="dataFim"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot" >
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-8">
                                <button id="btn-busca-cancelamento" class="btn btn-info">Buscar</button>
                                <button id="btn-limpar-filtro" class="btn btn-secondary">Limpar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="table-responsive" > 	
                    <div class="col-sm-12">
                        <table class="table table-striped table-bordered table-hover" id="table_list_cancelamentos" >
                            <thead>
                                <tr role="row">
                                    <th>Id</th>
                                    <th>Csid</th>
                                    <th>Nome Cliente</th>
                                    <th>Produto</th>
                                    <th>Motivo</th>
                                    <th>Status</th>
                                    <th>Data Contato</th>
                                    <th>Data Solicitação</th>
                                    <th data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell kt-datatable__cell--sort">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('view-utils.modal-interacoes')
@include('view-utils.modal-anexa-arquivo-carta')
@include('view-utils.modal-anexa-arquivo-os')
@include('view-utils.modal-detalhe-cancelamento')
@include('view-utils.modal-status-cancelamento')
@include('view-utils.modal-visualizar-arquivos') 

@endsection

@section('js')
    <script src="{{asset('js/cancelamento/index.js')}}"></script>
    <script src="{{asset('js/utils/modal-interacoes.js')}}"></script>
    <script src="{{asset('js/utils/modal-anexa-arquivo-carta.js')}}"></script>
    <script src="{{asset('js/utils/modal-anexa-arquivo-os.js')}}"></script>
    <script src="{{asset('js/utils/modal-detalhe-cancelamento.js')}}"></script>
    <script src="{{asset('js/utils/modal-status-cancelamento.js')}}"></script>
    <script src="{{asset('js/utils/modal-visualizar-arquivos.js')}}"></script> 
@endsection