<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\CancelamentoUsuarioRepository;
use App\Models\CancelamentoUsuario;
use App\Validators\CancelamentoUsuarioValidator;

/**
 * Class CancelamentoUsuarioRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CancelamentoUsuarioRepositoryEloquent extends BaseRepository implements CancelamentoUsuarioRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CancelamentoUsuario::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
