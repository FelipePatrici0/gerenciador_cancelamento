<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\CancelamentoRepository;
use App\Models\Cancelamento;
use App\Validators\CancelamentoValidator;

/**
 * Class CancelamentoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CancelamentoRepositoryEloquent extends BaseRepository implements CancelamentoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Cancelamento::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
