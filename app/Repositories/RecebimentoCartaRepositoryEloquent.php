<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\RecebimentoCartaRepository;
use App\Models\RecebimentoCarta;
use App\Validators\RecebimentoCartaValidator;

/**
 * Class RecebimentoCartaRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RecebimentoCartaRepositoryEloquent extends BaseRepository implements RecebimentoCartaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RecebimentoCarta::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
