<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\AcessoUsuarioRepository;
use App\Models\User;
use App\Validators\AcessoUsuarioValidator;

/**
 * Class AcessoUsuarioRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AcessoUsuarioRepositoryEloquent extends BaseRepository implements AcessoUsuarioRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
