<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\InteracaoCancelamentoRepository;
use App\Models\InteracaoCancelamento;
use App\Validators\InteracaoCancelamentoValidator;

/**
 * Class InteracaoCancelamentoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class InteracaoCancelamentoRepositoryEloquent extends BaseRepository implements InteracaoCancelamentoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return InteracaoCancelamento::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
