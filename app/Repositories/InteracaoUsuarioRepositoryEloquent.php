<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\InteracaoUsuarioRepository;
use App\Models\InteracaoUsuario;
use App\Validators\InteracaoUsuarioValidator;

/**
 * Class InteracaoUsuarioRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class InteracaoUsuarioRepositoryEloquent extends BaseRepository implements InteracaoUsuarioRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return InteracaoUsuario::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
