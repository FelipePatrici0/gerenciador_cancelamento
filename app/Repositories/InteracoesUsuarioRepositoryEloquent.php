<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\InteracoesUsuarioRepository;
use App\Models\InteracoesUsuario;
use App\Validators\InteracoesUsuarioValidator;

/**
 * Class InteracoesUsuarioRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class InteracoesUsuarioRepositoryEloquent extends BaseRepository implements InteracoesUsuarioRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return InteracoesUsuario::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
