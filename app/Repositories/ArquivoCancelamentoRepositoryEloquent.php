<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\ArquivoCancelamentoRepository;
use App\Models\ArquivoCancelamento;
use App\Validators\ArquivoCancelamentoValidator;

/**
 * Class ArquivoCancelamentoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ArquivoCancelamentoRepositoryEloquent extends BaseRepository implements ArquivoCancelamentoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ArquivoCancelamento::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
