<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\StatusCancelamentoRepository;
use App\Models\StatusCancelamento;
use App\Validators\StatusCancelamentoValidator;

/**
 * Class StatusCancelamentoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class StatusCancelamentoRepositoryEloquent extends BaseRepository implements StatusCancelamentoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return StatusCancelamento::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
