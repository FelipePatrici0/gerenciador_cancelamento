<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\BuscaClienteRepository;
use App\Models\BuscaCliente;
use App\Validators\BuscaClienteValidator;

/**
 * Class BuscaClienteRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BuscaClienteRepositoryEloquent extends BaseRepository implements BuscaClienteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BuscaCliente::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
