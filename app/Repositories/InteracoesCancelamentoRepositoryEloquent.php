<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\InteracoesCancelamentoRepository;
use App\Models\InteracoesCancelamento;
use App\Validators\InteracoesCancelamentoValidator;

/**
 * Class InteracoesCancelamentoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class InteracoesCancelamentoRepositoryEloquent extends BaseRepository implements InteracoesCancelamentoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return InteracoesCancelamento::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
