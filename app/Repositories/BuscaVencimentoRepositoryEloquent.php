<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\BuscaVencimentoRepository;
use App\Models\BuscaVencimento;
use App\Validators\BuscaVencimentoValidator;

/**
 * Class BuscaVencimentoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BuscaVencimentoRepositoryEloquent extends BaseRepository implements BuscaVencimentoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BuscaVencimento::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
