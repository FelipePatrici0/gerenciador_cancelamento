<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProdutoRepository.
 *
 * @package namespace App\Interfaces;
 */
interface ProdutoRepository extends RepositoryInterface
{
    //
}
