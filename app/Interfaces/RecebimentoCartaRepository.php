<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RecebimentoCartaRepository.
 *
 * @package namespace App\Interfaces;
 */
interface RecebimentoCartaRepository extends RepositoryInterface
{
    //
}
