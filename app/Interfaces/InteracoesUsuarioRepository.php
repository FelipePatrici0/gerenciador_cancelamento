<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InteracoesUsuarioRepository.
 *
 * @package namespace App\Interfaces;
 */
interface InteracoesUsuarioRepository extends RepositoryInterface
{
    //
}
