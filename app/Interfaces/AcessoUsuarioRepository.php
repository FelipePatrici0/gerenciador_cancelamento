<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AcessoUsuarioRepository.
 *
 * @package namespace App\Interfaces;
 */
interface AcessoUsuarioRepository extends RepositoryInterface
{
    //
}
