<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CancelamentoUsuarioRepository.
 *
 * @package namespace App\Interfaces;
 */
interface CancelamentoUsuarioRepository extends RepositoryInterface
{
    //
}
