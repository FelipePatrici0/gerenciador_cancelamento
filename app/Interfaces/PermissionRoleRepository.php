<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PermissionRoleRepository.
 *
 * @package namespace App\Interfaces;
 */
interface PermissionRoleRepository extends RepositoryInterface
{
    //
}
