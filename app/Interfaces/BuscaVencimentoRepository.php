<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BuscaVencimentoRepository.
 *
 * @package namespace App\Interfaces;
 */
interface BuscaVencimentoRepository extends RepositoryInterface
{
    //
}
