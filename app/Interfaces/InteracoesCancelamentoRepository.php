<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InteracoesCancelamentoRepository.
 *
 * @package namespace App\Interfaces;
 */
interface InteracoesCancelamentoRepository extends RepositoryInterface
{
    //
}
