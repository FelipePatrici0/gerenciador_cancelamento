<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BuscaClienteRepository.
 *
 * @package namespace App\Interfaces;
 */
interface BuscaClienteRepository extends RepositoryInterface
{
    //
}
