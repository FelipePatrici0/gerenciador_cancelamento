<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ClienteRepository.
 *
 * @package namespace App\Interfaces;
 */
interface ClienteRepository extends RepositoryInterface
{
    //
}
