<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InteracaoUsuarioRepository.
 *
 * @package namespace App\Interfaces;
 */
interface InteracaoUsuarioRepository extends RepositoryInterface
{
    //
}
