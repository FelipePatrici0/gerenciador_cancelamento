<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ArquivoCancelamentoRepository.
 *
 * @package namespace App\Interfaces;
 */
interface ArquivoCancelamentoRepository extends RepositoryInterface
{
    //
}
