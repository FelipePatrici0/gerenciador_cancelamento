<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InteracaoCancelamentoRepository.
 *
 * @package namespace App\Interfaces;
 */
interface InteracaoCancelamentoRepository extends RepositoryInterface
{
    //
}
