<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AquisicaoServicoRepository.
 *
 * @package namespace App\Interfaces;
 */
interface AquisicaoServicoRepository extends RepositoryInterface
{
    //
}
