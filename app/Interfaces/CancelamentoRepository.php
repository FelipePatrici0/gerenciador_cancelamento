<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CancelamentoRepository.
 *
 * @package namespace App\Interfaces;
 */
interface CancelamentoRepository extends RepositoryInterface
{
    //
}
