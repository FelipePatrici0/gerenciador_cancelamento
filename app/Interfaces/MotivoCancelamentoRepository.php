<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MotivoCancelamentoRepository.
 *
 * @package namespace App\Interfaces;
 */
interface MotivoCancelamentoRepository extends RepositoryInterface
{
    //
}
