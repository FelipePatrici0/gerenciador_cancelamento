<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleUserRepository.
 *
 * @package namespace App\Interfaces;
 */
interface RoleUserRepository extends RepositoryInterface
{
    //
}
