<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StatusCancelamentoRepository.
 *
 * @package namespace App\Interfaces;
 */
interface StatusCancelamentoRepository extends RepositoryInterface
{
    //
}
