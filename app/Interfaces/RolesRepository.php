<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RolesRepository.
 *
 * @package namespace App\Interfaces;
 */
interface RolesRepository extends RepositoryInterface
{
    //
}
