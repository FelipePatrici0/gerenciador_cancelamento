<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadFileRequest extends RequestJson
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {   
 
        return [
            'tipo_arquivo'         => 'required',
            'id_recebimento_carta' => 'required_if:tipo_arquivo,==,Carta',
            'contato'              => 'required_if:id_recebimento_carta,1,3',
            'arquivo'              => 'required|mimes:pdf,png,jpeg|max:1000',
        ];
       
    }

    function messages()
    {
        return [
            'tipo_arquivo.required'            => 'Campo tipo de arquivo é obrigatório.',
            'id_recebimento_carta.required_if' => 'É obrigatório definir o meio de recebimento do arquivo.',
            'contato.required_if'              => 'É obrigatório registrar o contato do cliente.',
            'arquivo.required'                 => 'É obrigatório anexar o arquivo.',
            'arquivo.mimes'                    => 'Apenas extensões pdf,png e jpeg são permitidas.',
        ];
    }
}
