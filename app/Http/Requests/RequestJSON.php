<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Response;

class RequestJSON extends FormRequest
{
    public function response(array $errors)
    {
        return Response::json([
            'success' => false,
            'message'  => $errors
        ]);
    }   
}
