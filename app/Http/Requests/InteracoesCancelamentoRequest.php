<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InteracoesCancelamentoRequest extends RequestJSON
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_cancelamento' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'id_cancelamento.required'  => 'É obrigatório informar o ID do cancelamento',
        ];
    }
}
