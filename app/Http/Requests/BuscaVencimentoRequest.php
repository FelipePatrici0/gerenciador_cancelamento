<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BuscaVencimentoRequest extends RequestJSON
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'csid' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'csid.required'  => 'Nenhum cliente foi selecionado...',
        ];
    }
}
