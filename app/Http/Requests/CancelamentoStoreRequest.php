<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CancelamentoStoreRequest extends RequestJSON
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'                      => 'required',
            'csid'                      => 'required',
            'inicio_contrato'           => 'required',
            'id_status_cancelamento'    => 'required',
            'id_produto'                => 'required',
            'id_motivo_cancelamento'    => 'required',
            'tipo_ligacao'              => 'required',

        ];
    }

    public function messages()
    {
        
        return[
           
            'name.required'                     => 'Campo nome é obrigatório',
            'csid.required'                     => 'Campo csid é obrigatório',
            'inicio_contrato.required'          => 'Campo inicio do contrato é obrigatório',
            'id_status_cancelamento.required'   => 'Preenchimento do status do cancelamento é obrigatório',
            'id_produto.required'               => 'Preenchimento do produto é obrigatório',
            'id_motivo_cancelamento.required'   => 'Preenchimento do motivo do cancelamento é obrigatório',
            'tipo_ligacao.required'             => 'Preenchimento do tipo de ligação é obrigatório',
            
        ];
    }
}
