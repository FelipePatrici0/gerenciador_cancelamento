<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateStatusCancelamentoRequest extends RequestJSON
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_status_cancelamento'  => 'required',
           
        ];
    }

    public function messages()
    {
        return [
            'id_status_cancelamento.required'   => "É Obrigatório definir o Status do Cancelamento."
        ];
    }
}
