<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateFileRequest extends RequestJson
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'arquivo'              => 'required|mimes:pdf,png,jpeg|max:1000',
        ];
    }

    public function messages()
    {
        return[
            'arquivo.required'                 => 'É obrigatório anexar o arquivo.',
            'arquivo.mimes'                    => 'Apenas extensões pdf,png e jpeg são permitidas.',
        ];
    }
}
