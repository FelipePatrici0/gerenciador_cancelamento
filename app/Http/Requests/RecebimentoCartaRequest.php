<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RecebimentoCartaRequest extends RequestJSON
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descricao' => 'required',
            'status'    => 'required'
        ];
    }

    function messages()
    {
        return [
            'descricao.required' => 'Campo descrição é obrigatório',
            'status.required'    => 'Campo status é obrigatório'
        ];
        
    }
}
