<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Support\Facades\Auth;
use App\Interfaces\AcessoUsuarioRepository;


class AuthController extends Controller
{   
    protected $repository;

    public function __construct(AcessoUsuarioRepository $repository){
        $this->repository = $repository;
    }

    public function index()
    {
        return view('login.login');
    }

    public function login(Request $request)
    {   
        try{
            $inputs = $request->only('email', 'password');
        
            $query = $this->repository->findWhere([
                'email' => $request->email
            ]);

            $credenciais = $query->first();
            
            $dataJson = [
                'message'   => ''
            ];

            if($credenciais){
                if($credenciais['status'] == 'D'){
                    $dataJson['message'] = 'Conta desativada no sistema, entrar em contato com o administrador.';
                    $dataJson['success'] = false;

                }else{
                    if(Auth::attempt($inputs)){
                        
                        $dataJson['message'] = 'Login realizado com sucesso!';
                        $dataJson['success'] = true;
        
                    }else{
                        $dataJson['message'] = 'Falha ao realizar login, verifique email e senha.';
                        $dataJson['success'] = false;
                    }
                }
            }else{
                $dataJson['message'] = 'Email não encontrado, certifique-se que já foi cadastrado.';
                $dataJson['success'] = false;
                
            }
            return response()->json($dataJson);   
            
        } catch(\Exception $e){
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
       
    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
       
    }


}
