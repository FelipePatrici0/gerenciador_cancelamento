<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BuscaVencimentoRequest;
use App\Http\Controllers\Controller;
use App\Interfaces\BuscaVencimentoRepository;

class BuscaVencimentosController extends Controller
{   
    protected $repository;

    public function __construct(BuscaVencimentoRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function diversos(BuscaVencimentoRequest $request)
    {   
       
        if (request()->wantsJson()) {
            $perPage = $request->length;
            if (isset($request->start)) {
                $total = $request->start / $perPage;
                $page = ($total + 1) > 0 ? $total + 1 : 1;
            } else {
                $page = 1;
            }

            $codigo = str_pad($request->csid, 6, "0", STR_PAD_LEFT);

            $request->merge([
                'page' => $page,
                'search' => $request->search['value'],
                'orderBy' => $request->columns[$request->order[0]['column']]['data'],
                'sortedBy' => $request->order[0]['dir'],
                'codigo' => $codigo
            ]);

            $this->repository->pushCriteria(app('\App\Criterias\RequestCriteria'));
            $data = $this->repository->scopeQuery(function ($query) use ($request) {
                $query = $query
                    ->join('VDR', function ($join) {
                        $join->on('VDR.CRE_CODIGO', '=', 'CRE.CODIGO')
                            ->on('VDR.EMP_CODIGO', '=', 'CRE.EMP_CODIGO');
                    })->join('CLI', function ($join) {
                        $join->on('CLI.CODIGO', '=', 'CRE.CLI_CODIGO')
                            ->on('CRE.EMP_CODIGO', '=', 'CLI.EMP_CODIGO');
                    })->leftJoin('BVD', function ($join) {
                        $join->on('BVD.EMP_Codigo', '=', 'VDR.EMP_Codigo')
                            ->on('BVD.VDR_CRE_Codigo', '=', 'VDR.CRE_Codigo')
                            ->on('BVD.VDR_Sequencial', '=', 'VDR.Sequencial')
                            ->whereNull('BVD.DtCancel');
                    })->leftJoin('ORL', function ($join) {
                        $join->on('ORL.CRE_CODIGO', '=', 'VDR.CRE_CODIGO')
                            ->on('ORL.EMP_CODIGO', '=', 'VDR.EMP_CODIGO')
                            ->on('ORL.BVD_VDR_SEQUENCIAL', '=', 'VDR.SEQUENCIAL')
                            ->where('ORL.LAN_ORIGEM', '=', 'D');
                    })->leftJoin('LAN', function ($join) {
                        $join->on('ORL.LAN_CODIGO', '=', 'LAN.CODIGO')
                            ->on('ORL.EMP_CODIGO', '=', 'LAN.EMP_CODIGO')
                            ->where('LAN.ORIGEM', '=', 'D');
                    })->leftJoin('CON', function ($join) {
                        $join->on('CON.CODIGO', '=', 'LAN.CON_CODIGO')
                            ->on('CON.EMP_CODIGO', '=', 'LAN.EMP_CODIGO');
                    })
                    ->select(\DB::raw('
                        CLI.NOME as NOME_CLI, 
                        CRE.CLI_CODIGO as CSID, 
                        CRE.VALOR as VALOR_CONTRATO, 
                        VDR.VALOR as VALOR_BOLETO, 
                        VDR.DTVENC, 
                        VDR.TITULO as OBSERVACAO,
                        CLI.VENCIMENTO as DIA_VENCIMENTO,
                        CLI.OBS as OBS_CLI, 
                        BVD.Data as DATA_PAGAMENTO, 
                        CRE.RENEGOCIADO  as RENEGOCIACAO,
                        CON.NOME as FORMA_PAGAMENTO, 
                        BVD.valor as VALOR_PAGO, 
                        BVD.Juros as JUROS,	
                        CON.codigo as COD_FORMA_PAGAMENTO,
                        VDR.ENVIADO, 
                        VDR.nrboleto,
                        CRE.codigo as contasreceber,
                        VDR.obs as obs_VDR,
                        VDR.DtCancel as dtcancelparcela
                '));
                return $query
                ->identic('CLI.CODIGO', $request->codigo)
                ->orderBy('VDR.DTVENC', 'DESC');
            })->paginate($perPage);

            return response()->json([
                'data' => $data->items(),
                'draw' => $request->draw,
                'recordsTotal' => $data->total(),
                'recordsFiltered' => $data->total(),
            ]);
        }
    }

    public function cartao(BuscaVencimentoRequest $request)
    {
        if (request()->wantsJson()) {

            $perPage = $request->length;
            if (isset($request->start)) {
                $total = $request->start / $perPage;
                $page = ($total + 1) > 0 ? $total + 1 : 1;
            } else {
                $page = 1;
            }

            $codigo = str_pad($request->csid, 6, "0", STR_PAD_LEFT);

            $request->merge([
                'page' => $page,
                'search' => $request->search['value'],
                'orderBy' => $request->columns[$request->order[0]['column']]['data'],
                'sortedBy' => $request->order[0]['dir'],
                'codigo' => $codigo
            ]);

            $this->repository->pushCriteria(app('\App\Criterias\RequestCriteria'));
            $data = $this->repository->scopeQuery(function ($query) use ($request) {
                $query =  $query
                    ->join('CLI', function ($join) {
                        $join->on('CLI.CODIGO', '=', 'CRE.CLI_CODIGO')
                            ->on('CRE.EMP_CODIGO', '=', 'CLI.EMP_CODIGO');
                    })->join('VCR', function ($join) {
                        $join->on('VCR.CRE_CODIGO', '=', 'CRE.CODIGO')
                            ->on('VCR.EMP_CODIGO', '=', 'CRE.EMP_CODIGO');
                    })->leftJoin('BVC', function ($join) {
                        $join->on('BVC.EMP_Codigo', '=', 'VCR.EMP_Codigo')
                            ->on('BVC.VCR_CRE_Codigo', '=', 'VCR.CRE_Codigo')
                            ->on('BVC.VCR_Sequencial', '=', 'VCR.Sequencial')
                            ->whereNull('BVC.DtCancel');
                    })->leftJoin('ORL', function ($join) {
                        $join->on('ORL.CRE_CODIGO', '=', 'VCR.CRE_CODIGO')
                            ->on('ORL.EMP_CODIGO', '=', 'VCR.EMP_CODIGO')
                            ->on('ORL.BVC_VCR_Sequencial', '=', 'VCR.SEQUENCIAL')
                            ->where('ORL.LAN_ORIGEM', '=', 'D');
                    })->leftJoin('LAN', function ($join) {
                        $join->on('ORL.LAN_CODIGO', '=', 'LAN.CODIGO')
                            ->on('ORL.EMP_CODIGO', '=', 'LAN.EMP_CODIGO')
                            ->where('LAN.ORIGEM', '=', 'D');
                    })->leftJoin('CON', function ($join) {
                        $join->on('CON.CODIGO', '=', 'LAN.CON_CODIGO')
                            ->on('CON.EMP_CODIGO', '=', 'LAN.EMP_CODIGO');
                    })->select(\DB::raw('
                        CLI.NOME as NOME_CLIENTE,
                        CRE.CLI_CODIGO as CSID,
                        CRE.VALOR as VALOR_CONTRATO,
                        VCR.VALOR as VALOR_BOLETO,
                        VCR.DTVENC as DATA_VENCIMENTO,
                        VCR.TITULO as OBSERVACAO,
                        CLI.VENCIMENTO as DIA_VENCIMENTO,
                        CLI.OBS as OBS_CLIENTE,
                        BVC.Data as DATA_PAGAMENTO,
                        CRE.RENEGOCIADO as RENEGOCIACAO,
                        CON.NOME as FORMA_PAGAMENTO,
                        BVC.Valor as VALOR_PAGO,
                        CON.codigo as COD_FORMA_PAGAMENTO, VCR.DtCancel as dtcancelparcela
                '));
                return $query
                ->identic('CLI.CODIGO', $request->codigo)
                ->orderBy('VCR.DTVENC', 'DESC');
            })->paginate($perPage);

            return response()->json([
                'data' => $data->items(),
                'draw' => $request->draw,
                'recordsTotal' => $data->total(),
                'recordsFiltered' => $data->total(),
            ]);
        }
    }
    
}
