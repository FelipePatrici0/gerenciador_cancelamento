<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Interfaces\ProdutoRepository;

class ProdutoController extends Controller
{   
    protected $repository;

    public function __construct(ProdutoRepository $repository){
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        if(request()->json()){
            $merge = [
                'orderBy' => 'descricao',
                'sortedBy' => 'asc',
            ];

            $request->merge($merge);

            $this->repository->pushCriteria(app('\App\Criterias\RequestCriteria'));
            $data = $this->repository->scopeQuery(function ($query) use ($request) {
                return  $query
                    ->where('status', 'A')
                    ->like('descricao', $request->parametro)
                    ->select(\DB::raw('produtos.*'));
            })->paginate(30);

            return response()->json([
                'data'  => $data->items(),
                'total' => $data->total()
            ]);

         
        }

    }
}
