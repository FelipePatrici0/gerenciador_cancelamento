<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\MotivoCancelamento;
use App\Interfaces\MotivoCancelamentoRepository;
use App\Http\Controllers\Controller;

class MotivoCancelamentoController extends Controller
{
    protected $repository;

    public function __construct(MotivoCancelamentoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {   
        if(request()->json()){
            $merge = [
                'orderBy' => 'descricao',
                'sortedBy' => 'asc',
            ];
    
            $request->merge($merge);
    
            $this->repository->pushCriteria(app('\App\Criterias\RequestCriteria'));
            $data = $this->repository->scopeQuery(function ($query) use ($request) {
                return  $query
                    ->where('status', 'A')
                    ->like('descricao', $request->parametro)
                    ->select(\DB::raw('motivo_cancelamentos.*'));
            })->paginate(30);
    
            return response()->json([
                'data'  => $data->items(),
                'total' => $data->total()
            ]);
        }

        $data = $this->repository->all();

        return view('motivoCancelamento.index', compact($data));
    }
   
    public function store(MotivoCancelamento $request)
    {
        try{    
            $data = $request->all();

            $status = $this->repository->create([
                'descricao'   => $data['descricao'],
                'status'      => $data['status'],
            ]);

            return response()->json([
                'sucess'  => true,
                'message' => 'Motivo de Cancelamento cadastrado com sucesso!',
            ], 201);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    }

    public function update(MotivoCancelamento $request)
    {   
        try{
            $data = $request->all();

            $status = $this->repository->update([
                'descricao' => $data['descricao'],
                'status'    => $data['status'],
            ], $data['id']);

            return response()->json([
                'sucess'  => true,
                'message' => 'Motivo de Cancelamento alterado com sucesso!',
            ], 201);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    }
    
}
