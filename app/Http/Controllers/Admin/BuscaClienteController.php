<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BuscaCliente;
use App\Http\Controllers\Controller;
use App\Interfaces\ClienteRepository;



class BuscaClienteController extends Controller
{
    protected $clienteRepository;

    public function __construct(
        ClienteRepository $clienteRepository

    ) {
        $this->clienteRepository = $clienteRepository;
    }

    public function index(Request $request)
    {

        $this->clienteRepository->pushCriteria(app('\App\Criterias\RequestCriteria'));
        $data = $this->clienteRepository->scopeQuery(function ($query) use ($request) {
            return $query
                ->join('SCL', function ($join) {
                    $join->on('CLI.Codigo', '=', 'SCL.CLI_Codigo')
                         ->on('CLI.EMP_Codigo', '=', 'SCL.EMP_Codigo');
                })
                ->join('SER', function ($join) {
                    $join->on('SER.Codigo', '=', 'SCL.SER_Codigo')
                         ->on('SER.EMP_Codigo', '=', 'SCL.EMP_Codigo');
                })
                ->join('GSE', function ($join) {
                    $join->on('GSE.Codigo', '=', 'SER.GSE_Codigo')
                         ->on('SER.EMP_Codigo', '=', 'GSE.EMP_Codigo')
                         ->where('GSE.Codigo', '=' ,'0003');
                })
                ->leftJoin('MUN', function ($join) {
                    $join->on('MUN.Codigo', '=', 'CLI.MUN_Codigo')
                         ->on('CLI.MUN_UFD_Sigla', '=', 'MUN.UFD_Sigla');
                    })
                    ->leftJoin('UFD', function ($join) {
                        $join->on('UFD.Sigla', '=', 'CLI.MUN_UFD_Sigla');
                    })
                    ->leftJoin('ECL', function ($join) {
                        $join->on('ECL.CLI_Codigo', '=', 'CLI.Codigo'); 
                })->like('CLI.' . $request->modo_query, $request->parametro)
                ->where('CLI.EMP_Codigo', '0001')
                ->select(\DB::raw("
                        Cli.EMP_Codigo,
                        CLI.Codigo as id,
                        CLI.Nome,
                        CLI.NomeFantasia,
                        CLI.CNPJCPF,
                        CLI.RG,
                        CLI.DDD_CELULAR,
                        CLI.Celular,
                        CLI.DDD,
                        CLI.Fone,
                        CLI.Email,
                        CLI.ENDCEP,
                        CLI.EndLogradouro,
                        CLI.EndNumero,
                        CLI.EndComplemento,
                        CLI.ENDBairro,
                        MUN.Nome as Cidade,
                        UFD.Nome as Estado,
                        ECL.PontoRef,
                        SCL.Aquisicao"))->orderBy('CLI.Nome', 'asc');
        })->paginate(20);
        
        return response()->json([
            'data'  => $data->items(),
            'total' => $data->total()
        ]);
}   

}
