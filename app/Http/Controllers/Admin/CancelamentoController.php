<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Cancelamento;
use App\Http\Requests\CancelamentoUpdateRequest;
use App\Http\Requests\CancelamentoStoreRequest;
use App\Http\Requests\UpdateStatusCancelamentoRequest;
use App\Http\Requests;
use Response;


use App\Http\Controllers\Controller;

use App\Interfaces\CancelamentoRepository;
use App\Interfaces\StatusCancelamentoRepository;
use App\Interfaces\MotivoCancelamentoRepository;
use App\Interfaces\RecebimentoCartaRepository;
use App\Interfaces\ProdutoRepository;


use App\Service\ServiceUser;
use App\Service\ServiceCancelamentoUsuario;
use App\Service\ServiceInteracoesCancelamento;

class CancelamentoController extends Controller
{   
    protected $repository;
    protected $statusCancelamentoRepository;
    protected $motivoCancelamentoRepository;
    protected $recebimentoCartaRepository;
    protected $produtoRepository;
    
    protected $serviceUser;
    protected $serviceCancelamentoUsuario;
    protected $serviceInteracoesCancelamento;
    

    public function __construct(
        CancelamentoRepository        $repository,
        StatusCancelamentoRepository  $statusCancelamentoRepository,
        MotivoCancelamentoRepository  $motivoCancelamentoRepository,
        RecebimentoCartaRepository    $recebimentoCartaRepository,
        ProdutoRepository             $produtoRepository,
        
        ServiceUser                   $serviceUser,
        ServiceCancelamentoUsuario    $serviceCancelamentoUsuario,
        ServiceInteracoesCancelamento $serviceInteracoesCancelamento

    ){
        $this->repository                    = $repository;
        $this->statusCancelamentoRepository  = $statusCancelamentoRepository;
        $this->motivoCancelamentoRepository  = $motivoCancelamentoRepository;
        $this->recebimentoCartaRepository    = $recebimentoCartaRepository;
        $this->produtoRepository             = $produtoRepository;

        $this->serviceUser                   = $serviceUser;
        $this->serviceCancelamentoUsuario    = $serviceCancelamentoUsuario;
        $this->serviceInteracoesCancelamento = $serviceInteracoesCancelamento;
    }


    public function index(Request $request)
    {   
       
        if(request()->wantsJson()){     
            $perPage = $request->length;
            if (isset($request->start)) {
                $total = $request->start / $perPage;
                $page = ($total + 1) > 0 ? $total + 1 : 1;
            } else {
                $page = 1;
            }

            $request->merge([
                'page' => $page,
                'search' => $request->search['value'],
                'orderBy' => $request->columns[$request->order[0]['column']]['data'],
                'sortedBy' => $request->order[0]['dir']
            ]);

            $this->repository->pushCriteria(app('\App\Criterias\RequestCriteria'));
            $data = $this->repository->with('arquivoCancelamento')->scopeQuery(function ($query) use ($request){
                $query = $query
                    ->join('status_cancelamentos','cancelamentos.id_status_cancelamento', '=', 'status_cancelamentos.id')
                    ->join('motivo_cancelamentos','cancelamentos.id_motivo_cancelamento', '=', 'motivo_cancelamentos.id')
                    ->leftjoin('produtos','cancelamentos.id_produto', '=', 'produtos.id')
                    ->leftjoin('recebimento_cartas','cancelamentos.id_recebimento_carta','=','recebimento_cartas.id')
                    ->where('cancelamentos.status', 'A')
                    ->select(
                        'cancelamentos.id',
                        'status_cancelamentos.descricao as statusCancelamento',
                        'motivo_cancelamentos.descricao as motivoCancelamento',
                        'motivo_cancelamentos.id as id_motivoCancelamento',
                        'recebimento_cartas.descricao as recebimentoCarta',
                        'produtos.descricao as produto',
                        'cancelamentos.nome',
                        'cancelamentos.csid',
                        'cancelamentos.placa_veiculo',
                        'cancelamentos.inicio_contrato',
                        'cancelamentos.data_solicitacao',
                        'cancelamentos.contato',
                        'cancelamentos.observacoes',
                        'cancelamentos.status',
                        'cancelamentos.id_status_cancelamento',
                        'cancelamentos.created_at',
                        'cancelamentos.tipo_ligacao'
                       
                    );

                if (isset($request->dataInicio) and !empty($request->dataInicio) and isset($request->dataFim) and !empty($request->dataFim)) {
                    $request->dataInicio  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->dataInicio)->format('Y-m-d');
                    $request->dataFim  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->dataFim)->format('Y-m-d');
                    $query->whereBetween('cancelamentos.data_solicitacao', [$request->dataInicio, $request->dataFim]);
                } 

                return  $query
                    ->identic('status_cancelamentos.id', $request->status_cancelamento)
                    ->identic('motivo_cancelamentos.id', $request->motivo_cancelamento)
                    ->orderBy('cancelamentos.id', 'DESC');

            })->paginate($perPage);
            
            return response()->json([
                'data' => $data->items(),
                'draw' => $request->draw,
                'recordsTotal' => $data->total(),
                'recordsFiltered' => $data->total(),
            ]);
        }
         
        return view('cancelamento.index'); 
    }

    public function create(Request $request)
    {
        $cancelamentoModel       = $this->repository->model();
        $cancelamento            = new $cancelamentoModel();
        
        $statusCancelamentoModel = $this->statusCancelamentoRepository->model();
        $statusCancelamento      = new $statusCancelamentoModel();

        $produtoModel            = $this->produtoRepository->model();
        $produto                 = new $produtoModel();

        $motivoCancelamentoModel = $this->motivoCancelamentoRepository->model();
        $motivoCancelamento      = new $motivoCancelamentoModel();
    
        return view('cancelamento.create' , compact('cancelamento','statusCancelamento','motivoCancelamento','produto'));
    }

    public function store(CancelamentoStoreRequest $request)
    {       

        try{      
            
            $query = $this->repository->findByField('csid', $request->csid);

            if(isset($query[0]['id_status_cancelamento']) && $query[0]['id_status_cancelamento']  == 1){
                
                return response()->json([
                    'success'  => false,
                    'message' => [
                        0 =>['Não foi possivel registrar, CSID já possue um cancelamento em andamento.']
                    ],
                ],201);

            }else{

                $data = $request->all();   

                $data['nome']                   = empty($request->nome)                   ? null : $request->nome;
                $data['csid']                   = empty($request->csid)                   ? null : $request->csid;              
                $data['inicio_contrato']        = empty($request->inicio_contrato)        ? null : \Carbon\Carbon::createFromFormat('d/m/Y', $request->inicio_contrato)->format('Y-m-d');
                $data['data_solicitacao']       = empty($request->data_solicitacao)       ? null : \Carbon\Carbon::createFromFormat('d/m/Y', $request->data_solicitacao)->format('Y-m-d');
                $data['placa_veiculo']          = empty($request->placa_veiculo)          ? null : $request->placa_veiculo;
                $data['tipo_ligacao']           = empty($request->tipo_ligacao)           ? null : $request->tipo_ligacao;  
                $data['id_produto']             = empty($request->id_produto)             ? null : $request->id_produto;        
                $data['id_motivo_cancelamento'] = empty($request->id_motivo_cancelamento) ? null : $request->id_motivo_cancelamento;
                $data['id_status_cancelamento'] = empty($request->id_status_cancelamento) ? null : $request->id_status_cancelamento;
                $data['observacoes']            = empty($request->observacoes)            ? null : $request->observacoes;
    
                $cancelamento = $this->repository->create($data);
    
                $user = $this->serviceUser->getUser();
                $idUser = $user['id'];
                
                $idCancelamento = $cancelamento->id;
    
                $this->serviceCancelamentoUsuario->register($idUser, $idCancelamento);
                $this->serviceInteracoesCancelamento->register($data['id_status_cancelamento'], $idCancelamento, $data['observacoes'], $idUser);
                
                return response()->json([
                    'success'  => true,
                    'message' => 'Cancelamento registrado com sucesso!',
                ], 200);
            }

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
      
    }
  
    public function edit(Request $request, $id)
    {
        $cancelamento = $this->repository->with(['motivoCancelamento', 'statusCancelamento', 'produto'])->find($id);
        
       
        $motivoCancelamento = $cancelamento->motivoCancelamento;
        $statusCancelamento = $cancelamento->statusCancelamento;
        $produto            = $cancelamento->produto;

        $cancelamento->inicio_contrato   = empty($cancelamento->inicio_contrato)  ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $cancelamento->inicio_contrato)->format('d/m/Y');
        $cancelamento->data_solicitacao  = empty($cancelamento->data_solicitacao) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $cancelamento->data_solicitacao)->format('d/m/Y');
       
        return view('cancelamento.edit', compact('cancelamento' , 'motivoCancelamento','statusCancelamento','produto'));
    }

    public function update(CancelamentoUpdateRequest $request, $id)
    {  

        try{
            $data = $request->all();

            $data['data_solicitacao']       = empty($request->data_solicitacao)       ? null : \Carbon\Carbon::createFromFormat('d/m/Y', $request->data_solicitacao)->format('Y-m-d');
            $data['placa_veiculo']          = empty($request->placa_veiculo)          ? null : $request->placa_veiculo;
            $data['tipo_ligacao']           = empty($request->tipo_ligacao)           ? null : $request->tipo_ligacao;
            $data['id_produto']             = empty($request->id_produto)             ? null : $request->id_produto;
            $data['id_motivo_cancelamento'] = empty($request->id_motivo_cancelamento) ? null : $request->id_motivo_cancelamento;
            $data['observacoes']            = empty($request->observacoes)            ? null : $request->observacoes;
            
            $formData =  [
                'data_solicitacao'       => $data['data_solicitacao'],
                'placa_veiculo'          => $data['placa_veiculo'],
                'tipo_ligacao'           => $data['tipo_ligacao'],
                'id_produto'             => $data['id_produto'],
                'id_motivo_cancelamento' => $data['id_motivo_cancelamento'],
                'observacoes'            => $data['observacoes']  
            ];

            $cancelamento = $this->repository->update($formData, $id);

            return response()->json([
                'success' => true,
                'message' => 'Cancelamento alterado com sucesso!',
            ], 200);    

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    }

    public function updateStatus(UpdateStatusCancelamentoRequest $request, $id)
    {
        try{

       
            $idStatusCancelamento = $request->id_status_cancelamento;
            $observacoes          = $request->observacoes;

            $cancelamento = $this->repository->find($id);
            $cancelamento->id_status_cancelamento = $idStatusCancelamento;
            $cancelamento->save();
        
            $user = $this->serviceUser->getUser();
            $idUser = $user['id'];

            $this->serviceInteracoesCancelamento->register($idStatusCancelamento, $id, $observacoes, $idUser);

            return response()->json([
                'success' => true,
                'message' => 'Status alterado com sucesso!',
            ], 200);  

        }catch (\Exception $e){
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    }

   
}
