<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UploadFileRequest;
use App\Http\Requests\UpdateFileRequest;

use App\Interfaces\CancelamentoRepository;
use App\Interfaces\ArquivoCancelamentoRepository;

use App\Service\ServiceUser;

use App\Http\Controllers\Controller;

class ArquivosCancelamentoController extends Controller
{   
    protected $repository;
    protected $arquivoCancelamentoRepository;
    protected $serviceUser;

    public function __construct(   
        CancelamentoRepository        $repository,
        ArquivoCancelamentoRepository $arquivoCancelamentoRepository,
        ServiceUser                   $serviceUser
    ){
        $this->repository                    = $repository;
        $this->arquivoCancelamentoRepository = $arquivoCancelamentoRepository;
        $this->serviceUser                   = $serviceUser;
    }
    
    public function uploadFile(UploadFileRequest $request, $id)
    {

        try{   
            
            if(!empty($request->file('arquivo'))){
                
                if($request->hasFile('arquivo') && $request->file('arquivo')->isValid()){

                    $tipoArquivo = $request->tipo_arquivo == "Carta" ? "Carta" : "Os";

                    $time = $this->currentDateTime();

                    $user = $this->serviceUser->getUser('id');
                    $idUser = $user['id'];
                    
                    $diretorio = 'Arquivos/'.$tipoArquivo.'/';

                    $nameFile = $time.$id.$idUser.'.'.$request->file('arquivo')->extension();
                    $uploadUrl = $request->file('arquivo')->move($diretorio, $nameFile);

                    if(!$uploadUrl){
                        return response()->json([
                            'success' => false,
                            'message' => [
                                0 => ['Erro ao tentar salvar o arquivo!']
                            ],
                        ], 200);
                    }

                }else{
                    return response()->json([
                        'success'  => false,
                        'message' => [
                            0 => ['Falha! Arquivo possivelmente corrrompido.',]
                        ],
                    ], 200);
                }
            }
            
            if($tipoArquivo === 'Carta'){
                
                $data['id_cancelamento']    = $id;
                $data['tipo_arquivo']       = $request->tipo_arquivo;
                $data['arquivo']            = empty($nameFile)              ? null : $nameFile;
                $data['observacoes']        = empty($request->observacoes)  ? null : $request->observacoes;
                
                $this->arquivoCancelamentoRepository->create($data);
    
                $dataCancelamento['id_recebimento_carta']   = $request->id_recebimento_carta;
                $dataCancelamento['contato']                = empty($request->contato) ? null  : $request->contato;
    
                $this->repository->update($dataCancelamento, $id);
            }else{

                $data['id_cancelamento']    = $id;
                $data['tipo_arquivo']       = $request->tipo_arquivo;
                $data['arquivo']            = empty($nameFile)              ? null : $nameFile;
                $data['observacoes']        = empty($request->observacoes)  ? null : $request->observacoes;
                
                $this->arquivoCancelamentoRepository->create($data);
            }

            return response()->json([
                'success'  => true,
                'message' => 'Registro salvo com sucesso!',
            ], 200);

        }catch (\Exception $e){
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
        
    }
   
    public function updateFile(UpdateFileRequest $request, $id)
    {   
        try{
   
            if(!empty($request->file('arquivo'))){
                
                if($request->hasFile('arquivo') && $request->file('arquivo')->isValid()){
                    $tipoArquivo = $request->tipo_arquivo == "Carta" ? "Carta" : "Os";

                    $time = $this->currentDateTime();

                    $user = $this->serviceUser->getUser('id');
                    $idUser = $user['id'];
                    
                    $diretorio = 'Arquivos/'.$tipoArquivo.'/';

                    $nameFile = $time.$id.$idUser.'.'.$request->file('arquivo')->extension();
                    $uploadUrl = $request->file('arquivo')->move($diretorio, $nameFile);

                    if(!$uploadUrl){
                        return response()->json([
                            'success' => false,
                            'message' => [
                                0 =>[ 'Erro ao tentar salvar o arquivo!']
                            ],
                        ], 200);
                    }
             
                }else{
                    return response()->json([
                        'success'  => false,
                        'message' => [
                            0 => ['Falha! Arquivo possivelmente corrrompido.']
                        ],
                    ], 200);
                }
            }
            
            $data['arquivo'] = empty($nameFile) ? null : $nameFile;

            $this->arquivoCancelamentoRepository->update($data, $id);

            return response()->json([
                'success'  => true,
                'message' => 'Registro alterado com sucesso!',
            ], 201);

        }catch (\Exception $e){
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    }

    public function currentDateTime()
    {
        date_default_timezone_set('America/Fortaleza');
        $date = date('YmdHis');
        return $date;

    }


}
