<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Interfaces\RolesRepository;
use App\Interfaces\PermissionRepository;

class PerfisController extends Controller
{   
    protected $rolesRepository;
    protected $permissionRepository;

    public function __construct(
        RolesRepository         $rolesRepository,
        PermissionRepository    $permissionRepository
    ){
        $this->rolesRepository       = $rolesRepository;
        $this->permissionRepository  = $permissionRepository;
    }

    public function index(Request $request)
    {   
        if (request()->wantsJson()) {
            $perPage = $request->length;
            if (isset($request->start)) {
                $total = $request->start / $perPage;
                $page = ($total + 1) > 0 ? $total + 1 : 1;
            } else {
                $page = 1;
            }

            $request->merge([
                'page' => $page,
                'search' => $request->search['value'],
                'orderBy' => $request->columns[$request->order[0]['column']]['data'],
                'sortedBy' => $request->order[0]['dir']
            ]);

            $this->rolesRepository->pushCriteria(app('\App\Criterias\RequestCriteria'));
            $data = $this->rolesRepository->scopeQuery(function ($query) use ($request) {
                return $query
                    ->select('roles.*')
                    ->orderBy('id', 'desc');
            })->paginate($perPage);

            return response()->json([
                'data' => $data->items(),
                'draw' => $request->draw,
                'recordsTotal' => $data->total(),
                'recordsFiltered' => $data->total(),
            ]);
        }   
 
        return view('perfil.index');
    }

    public function create()
    {   
        $perfilModel = $this->rolesRepository->model();
        $perfil = new $perfilModel();

        $permissoes = $this->permissionRepository->orderBy('name','asc')->get();
        $permissoesPerfil = (object) [];
        
        
        return view('perfil.create', compact('perfil','permissoes','permissoesPerfil'));
    }

    public function store()
    {

    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }


}
