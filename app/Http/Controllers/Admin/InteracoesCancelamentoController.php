<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Interfaces\InteracoesCancelamentoRepository;
use App\Http\Requests\InteracoesCancelamentoRequest;

class InteracoesCancelamentoController extends Controller
{
    
    protected $repository;

    public function __construct(InteracoesCancelamentoRepository $repository)
    {
        $this->repository = $repository;
    }
 
    public function index(InteracoesCancelamentoRequest $request)
    {   

        $perPage = $request->length;
        if (isset($request->start)) {
            $total = $request->start / $perPage;
            $page = ($total + 1) > 0 ? $total + 1 : 1;
        } else {
            $page = 1;
        }

        $request->merge([
            'page' => $page,
            'search' => $request->search['value'],
            'orderBy' => $request->columns[$request->order[0]['column']]['data'],
            'sortedBy' => $request->order[0]['dir']
        ]);

        $this->repository->pushCriteria(app('\App\Criterias\RequestCriteria'));
        $data = $this->repository->scopeQuery(function ($query) use ($request) {
            return $query
                ->join('interacoes_usuarios', 'interacoes_usuarios.id_interacoes_cancelamento','=', 'interacoes_cancelamentos.id')
                ->join('users', 'interacoes_usuarios.id_usuario','=', 'users.id')
                ->join('status_cancelamentos','status_cancelamentos.id','=','interacoes_cancelamentos.id_status_cancelamento')
                ->identic('interacoes_cancelamentos.id_cancelamento', $request->id_cancelamento)
                ->select('interacoes_cancelamentos.id', 'users.name as usuario', 'status_cancelamentos.descricao as status', 'interacoes_cancelamentos.observacoes', 'interacoes_cancelamentos.created_at')
                ->orderBy('interacoes_cancelamentos.id', 'ASC');
        })->paginate($perPage);

        return response()->json([
            'data' => $data->items(),
            'draw' => $request->draw,
            'recordsTotal' => $data->total(),
            'recordsFiltered' => $data->total(),
        ]);
    }
    
}
