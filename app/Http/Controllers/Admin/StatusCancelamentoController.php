<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StatusCancelamento;
use App\Http\Controllers\Controller;
use App\Interfaces\StatusCancelamentoRepository;

class StatusCancelamentoController extends Controller
{   

    protected $repository;

    public function __construct(StatusCancelamentoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {   
        if(request()->json()){
            $merge = [
                'orderBy' => 'descricao',
                'sortedBy' => 'asc',
            ];
    
            $request->merge($merge);
    
            $this->repository->pushCriteria(app('\App\Criterias\RequestCriteria'));
            $data = $this->repository->scopeQuery(function ($query) use ($request) {
                return  $query
                    ->where('status', 'A')
                    ->like('descricao', $request->parametro)
                    ->select(\DB::raw('status_cancelamentos.*'));
            })->paginate(30);
    
            return response()->json([
                'data'  => $data->items(),
                'total' => $data->total()
            ]);
        }

        $data = $this->repository->all();

        return view('statusCancelamento.index', compact($data));
    }
   
    public function store(StatusCancelamento $request)
    {
        try{    
            $data = $request->all();

            $status = $this->repository->create([
                'descricao' => $data['descricao'],
                'status'      => $data['status'],
            ]);

            return response()->json([
                'sucess'  => true,
                'message' => 'Status cadastrado com sucesso!',
            ], 200);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    }

    public function update(StatusCancelamento $request)
    {   
        try{

            dd($data = $request->all());

            $status = $this->repository->update([
                'descricao' => $data['descricao'],
                'status'   => $data['status'],
            ], $data['id']);

            return response()->json([
                'sucess'  => true,
                'message' => 'Status alterado com sucesso!',
            ], 201);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    }

    
}
