<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Routas de Acesso
Route::get('/',       'Login\AuthController@index')->name('login.index');
Route::get('/login',  'Login\AuthController@index')->name('login.index');
Route::post('/login', 'Login\AuthController@login')->name('login.login');

//Route::get('/cliente', 'Admin\BuscaClienteController@index');

route::group(['middleware' =>['auth']], function(){

    //Dashboard
    Route::get('/home', 'Admin\DashboardController@index')->name('dashboard');

    //CRUD Status Cancelamento.
    Route::get('/status-cancelamento',          'Admin\StatusCancelamentoController@index')->name('status-cancelamento.index');
    Route::post('/status-cancelamento',         'Admin\StatusCancelamentoController@store')->name('status-cancelamento.store');
    Route::post('/status-cancelamento/update',  'Admin\StatusCancelamentoController@update')->name('status-cancelamento.update');
    Route::get('/status-cancelamento/search',   'Admin\StatusCancelamentoController@search')->name('status-cancelamento.search');
    
    //CRUD Recebimento Carta
    Route::get('/carta-cancelamento',           'Admin\RecebimentoCartaController@index')->name('carta-cancelamento.index');
    Route::post('/carta-cancelamento',          'Admin\RecebimentoCartaController@store')->name('carta-cancelamento.store');
    Route::post('/carta/cancelamento/update',   'Admin\RecebimentoCartaController@update')->name('carta-cancelamento.update');

    //CRUD Motivo Cancelamento
    Route::get('/motivo-cancelamento',           'Admin\MotivoCancelamentoController@index')->name('motivo-cancelamento.index');
    Route::post('/motivo-cancelamento',          'Admin\MotivoCancelamentoController@store')->name('motivo-cancelamento.store');
    Route::post('/motivo-cancelamento/update',   'Admin\MotivoCancelamentoController@update')->name('motivo-cancelamento.update');
    Route::get('/motivo-cancelamento/search',    'Admin\MotivoCancelamentoController@search')->name('motivo-cancelamento.search');

    //CRUD Cancelamento
    Route::get('/cancelamento',                        'Admin\CancelamentoController@index')->name('cancelamento.index');
    Route::get('/cancelamento/create',                 'Admin\CancelamentoController@create')->name('cancelamento.create');
    Route::post('/cancelamento',                       'Admin\CancelamentoController@store')->name('cancelamento.store');
    Route::get('/cancelamento/{id}/edit',              'Admin\CancelamentoController@edit')->name('cancelamento.edit');
    Route::put('/cancelamento/{id}',                   'Admin\CancelamentoController@update')->name('cancelamento.update');
    Route::post('/cancelamento/status/update/{id}',    'Admin\CancelamentoController@updateStatus')->name('cancelamento.updateStatus');
    Route::post('/cancelamento/arquivo/{id}',          'Admin\ArquivosCancelamentoController@uploadFile')->name('cancelamento.uploadFile');
    Route::post('/cancelamento/arquivo/update/{id}',   'Admin\ArquivosCancelamentoController@updateFile')->name('cancelamento.updateFile');

    //Interações Cancelamento
    Route::get('/cancelamento/interacoes/list',        'Admin\InteracoesCancelamentoController@index')->name('cancelamento.interacoes');

    //CRUD Usuarios
    Route::get('/usuarios',                             'Admin\UserController@index')->name('usuarios.index');
    Route::get('/usuario/create',                       'Admin\UserController@create')->name('usuario.create');
    Route::post('/usuario/store',                       'Admin\UserController@store')->name('usuario.store');
    Route::get('/usurio/{id}/edit',                     'Admin\UserController@edit')->name('usario.edit');
    Route::put('/usuario/{id}',                         'Admin\UserController@update')->name('usuario.update');

    //Perfis e Permissões
    Route::group(['prefix' => 'admin'], function (){
        Route::get('/perfis',                            'Admin\PerfisController@index')->name('perfil.index');
        Route::get('/perfil/create',                     'Admin\PerfisController@create')->name('perfil.create');
        Route::post('/perfil/store',                     'Admin\PerfisController@store')->name('perfil.store');
        Route::get('/perfil/{id}/edit',                  'Admin\PerfisController@edit')->name('perfil.edit');
        Route::put('/perfil/{id}',                       'Admin\PerfisController@update')->name('perfil.update');

    });

    //Busca Cliente
    Route::get('/busca/cliente',                        'Admin\BuscaClienteController@index')->name('busca.cliente.index');

    //Busca Produto
    Route::get('/busca/produto',                        'Admin\ProdutoController@index')->name('produto.index');

    //Busca Vencimentos 
    Route::get('/busca/vencimento/diversos',            'Admin\BuscaVencimentosController@diversos')->name('vecimento.diversos');
    Route::get('/busca/vencimento/cartao',              'Admin\BuscaVencimentosController@cartao')->name('vecimento.cartao');

    //Logout
    Route::get ('/logout', 'Login\AuthController@logout')->name('logout');

});

