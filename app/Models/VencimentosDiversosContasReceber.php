<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VencimentosDiversosContasReceber extends BaseModel
{
    
    protected $connection = 'connection_fortes';

    public $table = 'dbo.VDR';

    protected $fillable = [];

}
