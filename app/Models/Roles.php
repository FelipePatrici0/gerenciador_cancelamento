<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Roles.
 *
 * @package namespace App\Models;
 */
class Roles extends BaseModel implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = 'roles';

    protected $fillable = [
        'id',
        'name',
        'description'
    ];

}
