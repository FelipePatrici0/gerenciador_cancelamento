<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Produto.
 *
 * @package namespace App\Models;
 */
class Produto extends BaseModel implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'descricao',
        'status'
    ];

    public function cancelamentos(){
        return $this->hasMany("App\Models\Cancelamento",'id_produto', 'id');
    }
}
