<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class MotivoCancelamento.
 *
 * @package namespace App\Models;
 */
class MotivoCancelamento extends BaseModel implements Transformable
{
    use TransformableTrait;

    public $timestamps= false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'descricao',
        'status'
    ];
    
    public function cancelamentos(){
        return $this->hasMany("App\Models\Cancelamento", 'id_motivo_cancelamento', 'id');

    }

}
