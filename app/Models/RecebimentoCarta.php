<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RecebimentoCarta.
 *
 * @package namespace App\Models;
 */
class RecebimentoCarta extends BaseModel implements Transformable
{
    use TransformableTrait;

    public $timestamps= false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'descricao',
        'status'
    ];

    public function Cancelamentos(){
        return $this->hasMany("App\Models\Cancelamento", 'id_recebimento_carta', 'id');

    }
}
