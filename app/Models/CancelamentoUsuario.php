<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class CancelamentoUsuario.
 *
 * @package namespace App\Models;
 */
class CancelamentoUsuario extends BaseModel implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_cancelamento',
        'id_usuario',
        'status'
    ];

    public function cancelamento(){
        return $this->belongsTo("App\Models\Cancelamento", 'id_cancelamento', 'id');
    }

    public function user(){
        return $this->belongsTo("App\Models\User", 'id_usuario', 'id');
    }

}
