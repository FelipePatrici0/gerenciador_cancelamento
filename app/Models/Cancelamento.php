<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Cancelamento.
 *
 * @package namespace App\Models;
 */
class Cancelamento extends BaseModel implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_status_cancelamento',
        'id_motivo_cancelamento',
        'id_recebimento_carta',
        'id_produto',
        'nome',
        'csid',
        'placa_veiculo',
        'inicio_contrato',
        'data_solicitacao',
        'contato',
        'observacoes',
        'tipo_ligacao',
        'status'
    ];

    public function statusCancelamento(){
        return $this->belongsTo("App\Models\StatusCancelamento", 'id_status_cancelamento', 'id');

    }

    public function motivoCancelamento(){
        return $this->belongsTo("App\Models\MotivoCancelamento", 'id_motivo_cancelamento', 'id');
 
    }

    public function recebimentoCarta(){
        return $this->belongsTo("App\Models\RecebimentoCarta", 'id_recebimento_carta', 'id');

    }

    public function arquivoCancelamento(){
        return $this->hasMany("App\Models\ArquivoCancelamento", 'id_cancelamento', 'id');

    }

    public function cancelamentoUsuarios(){
        return $this->hasMany("App\Models\CancelamentoUsuario",'id_cancelamento', 'id');

    }
    
    public function interacoesCancelamentos(){
        return $this->hasMany("App\Models\InteracoesCancelamento",'id_cancelamento', 'id');

    }

    public function produto(){
        return $this->belongsTo("App\Models\Produto",'id_produto','id');
    }

   

    

    

    


    
}
