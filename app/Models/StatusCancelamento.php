<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class StatusCancelamento.
 *
 * @package namespace App\Models;
 */
class StatusCancelamento extends BaseModel implements Transformable
{
    use TransformableTrait;

    public $timestamps= false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'descricao',
        'status'
    ];

    public function interacoesCancelamentos(){
        return $this->hasMany("App\Models\InteracoesCancelamento",'id_status_cancelamento', 'id');

    }

    public function cancelamentos(){
        return $this->hasMany("App\Models\Cancelamento",'id_status_cancelamento', 'id');

    }

}
