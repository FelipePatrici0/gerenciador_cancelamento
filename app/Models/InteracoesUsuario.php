<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class InteracoesUsuario.
 *
 * @package namespace App\Models;
 */
class InteracoesUsuario extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_interacoes_cancelamento',
        'id_usuario',
        'status'
    ];

    public function interacoesCancelamento(){
        return $this->belongsTo("App\Models\InteracoesCancelamento",'id_interacoes_cancelamento', 'id');
    }

    public function user(){
        return $this->belongsTo("App\Models\User",'id_usuario', 'id');
    }
}
