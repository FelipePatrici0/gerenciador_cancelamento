<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class BuscaVencimento.
 *
 * @package namespace App\Models;
 */
class BuscaVencimento extends BaseModel implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'connection_fortes'; 

    public $table = 'dbo.CRE';

    public $timestamps = false;

}
