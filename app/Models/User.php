<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function cancelamentoUsuario(){
        return $this->hasMany("App\Models\CancelamentoUsuario",'id_usuario', 'id');

    }

    public function interecoesUsuarios(){
        return $this->hasMany("App\Models\InteracoesCancelamento",'id_usuario', 'id');
    }

}
