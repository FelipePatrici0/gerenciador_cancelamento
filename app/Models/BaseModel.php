<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {

    public function scopeLike($query, $campo, $valor){
        if(isset($valor) and !empty($valor)){
            return $query->where($campo, 'LIKE', "%{$valor}%");
        }
        else{
            return $query;
        }
    }

	public function scopeOrLike($query, $campo, $valor){
        if(isset($valor) and !empty($valor)){
            return $query->orWhere($campo, 'LIKE', "%{$valor}%");
        }
        else{
            return $query;
        }
    }

    public function scopeAtivo($query){
        return $query->where('status', '=', 'A');
    }


    public function scopeIdentic($query, $campo, $valor){
        if(isset($valor) and !empty($valor)){
            return $query->where($campo, '=', $valor);
        }
        else{
            return $query;
        }
    }

    public function scopeOrIdentic($query, $campo, $valor){
        if(isset($valor) and !empty($valor)){
            return $query->orWhere($campo, '=', $valor);
        }
        else{
            return $query;
        }
    }

    private static function mask($mascara, $string){
        $string = @str_replace(" ","",$string);
        $string = @preg_replace('/\D/', '', $string);
        for($i=0;$i<strlen($string);$i++){
            @$mascara[strpos($mascara,"#")] = $string[$i];
        }
        $mascara = @str_replace("#","_",$mascara);
        return $mascara;
    }

    public function maskCNPJ($campo){
        $string = $this->$campo;
        return self::mask('##.###.###/####-##', $string);
    }

    public function maskCPF($campo){
        $string = $this->$campo;
        return self::mask('###.###.###-##', $string);
    }

    public function maskCEP($campo){
        $string = $this->$campo;
        return self::mask('##.###-###', $string);
    }

    public function maskPhone($campo){
        $string = $this->$campo;
        if(strlen($string)==11){
            $mask = '(##) # ####-####';
        }
        else{
            $mask = '(##) ####-####';               
        }
        return self::mask($mask, $string);
    }
    
    public function verificarRelacionamentosRestritos(){
        return true;
    }

    public function deletarRelacoesCascade(){
        return true;
    }

    public function verificarDelete(){
        if($this->verificarRelacionamentosRestritos()==true){
            $this->deletarRelacoesCascade();
            $this->delete();
            return true;
        }
        else{
            return false;
        }
    }
}