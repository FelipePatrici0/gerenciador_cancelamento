<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class InteracoesCancelamento.
 *
 * @package namespace App\Models;
 */
class InteracoesCancelamento extends BaseModel implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_status_cancelamento',
        'id_cancelamento',
        'observacoes'

    ];

    public function statusCancelamento(){
        return $this->belongsTo("App\Models\StatusCancelamento",'id_status_cancelamentos', 'id');

    }

    public function cancelamento(){
        return $this->belongsTo("App\Models\Cancelamento", 'id_cancelamento', 'id');

    }

    public function interacoesUsuarios(){
        return $this->hasMany("App\Models\InteracoesUsuario", 'id_interacoes_cancelamento', 'id');
    }


}
