<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ArquivoCancelamento.
 *
 * @package namespace App\Models;
 */
class ArquivoCancelamento extends BaseModel implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_cancelamento',
        'tipo_arquivo',
        'arquivo',
        'observacoes',
        'status'
    ];

    public function cancelamento(){
        return $this->belongsTo("App\Models\Cancelamento", 'id_cancelamento', 'id');
    }
    

}
