<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AquisicaoServico.
 *
 * @package namespace App\Models;
 */
class AquisicaoServico extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $connection = 'connection_fortes';

    Public $table = 'dbo.SCL';

    protected $fillable = [];

}
