<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Cliente.
 *
 * @package namespace App\Models;
 */
class Cliente extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $connection = 'connection_fortes';

    public $table = 'dbo.CLI';

    protected $fillable = [];

}
