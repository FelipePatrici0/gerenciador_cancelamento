<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataPagamento extends BaseModel
{
    protected $connection = 'connection_fortes';

    public $table = 'dbo.BVD';

    protected $fillable = [];
}
