<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aquisicao extends BaseModel
{
    use TransformableTrait;

    protected $connection = 'connection_fortes';

    public $table = 'dbo.SCL';

    protected $fillable = [];
}
