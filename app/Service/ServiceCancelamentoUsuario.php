<?php

namespace App\Service;

use Illuminate\Http\Request;

use App\Interfaces\CancelamentoUsuarioRepository;

class ServiceCancelamentoUsuario
{   
    protected $repository;

    public function __construct(CancelamentoUsuarioRepository $repository)
    {
        $this->repository = $repository;
    }

    public function register($idUser, $idCancelamento)
    {
        try{

            $registroCancelamento = $this->repository->create([
                'id_cancelamento' => $idCancelamento,
                'id_usuario'      => $idUser,
            ]);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    }
}