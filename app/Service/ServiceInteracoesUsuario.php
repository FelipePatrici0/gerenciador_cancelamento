<?php 

namespace App\Service;

use Illuminate\Http\Request;

use App\Interfaces\InteracoesUsuarioRepository;

class ServiceInteracoesUsuario
{
    protected $repository;

    public function __construct(InteracoesUsuarioRepository $repository)
    {
        $this->repository = $repository;
    }

    public function register($idInteracaoCancelamento, $idUser)
    {
        try{
           
            $interacaoCancelamento = $this->repository->create([
                'id_interacoes_cancelamento' => $idInteracaoCancelamento,
                'id_usuario'                 => $idUser,
            ]);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    
    }


}