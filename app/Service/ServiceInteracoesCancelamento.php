<?php 

namespace App\Service;

use Illuminate\Http\Request;

use App\Interfaces\InteracoesCancelamentoRepository;

use App\Service\ServiceInteracoesUsuario;

class ServiceInteracoesCancelamento
{
    protected $repository;
    protected $serviceInteracoesUsuario;

    public function __construct(InteracoesCancelamentoRepository $repository, ServiceInteracoesUsuario $serviceInteracoesUsuario)
    {
        $this->repository = $repository;
        $this->serviceInteracoesUsuario = $serviceInteracoesUsuario;
    }

    public function register($idStatus, $idCancelamento, $observacoes, $idUser)
    {
        try{

            $interacaoCancelamento = $this->repository->create([
                'id_status_cancelamento' => $idStatus,
                'id_cancelamento'        => $idCancelamento,
                'observacoes'            => $observacoes,
            ]);

            $idInteracaoCancelamento = $interacaoCancelamento->id;

            $this->serviceInteracoesUsuario->register($idInteracaoCancelamento, $idUser);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => $e->getMessage()
            ], 500);
        }
    
    }


}