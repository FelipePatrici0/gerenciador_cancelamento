<?php

namespace App\Service;

class ServiceUser
{

    public function getUser()
    {
        
        $user = \Auth::User();
        
        return[
            'id'        => $user->id,
            'name'      => $user->name,
            'email'     => $user->email,
            'status'    => $user->status
        ];
    }

}