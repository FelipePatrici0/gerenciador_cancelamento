<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\ArquivoCancelamentoRepository;
use App\Repositories\ArquivoCancelamentoRepositoryEloquent;
use App\Interfaces\CancelamentoRepository;
use App\Repositories\CancelamentoRepositoryEloquent;
use App\Interfaces\CancelamentoUsuarioRepository;
use App\Repositories\CancelamentoUsuarioRepositoryEloquent;
use App\Interfaces\ClienteRepository;
use App\Repositories\ClienteRepositoryEloquent;
use App\Interfaces\InteracoesCancelamentoRepository;
use App\Repositories\InteracoesCancelamentoRepositoryEloquent;
use App\Interfaces\InteracoesUsuarioRepository;
use App\Repositories\InteracoesUsuarioRepositoryEloquent;
use App\Interfaces\MotivoCancelamentoRepository;
use App\Repositories\MotivoCancelamentoRepositoryEloquent;
use App\Interfaces\RecebimentoCartaRepository;
use App\Repositories\RecebimentoCartaRepositoryEloquent;
use App\Interfaces\StatusCancelamentoRepository;
use App\Repositories\StatusCancelamentoRepositoryEloquent;
use App\Interfaces\AcessoUsuarioRepository;
use App\Repositories\AcessoUsuarioRepositoryEloquent;
use App\Interfaces\ProdutoRepository;
use App\Repositories\ProdutoRepositoryEloquent;
use App\Interfaces\BuscaVencimentoRepository;
use App\Repositories\BuscaVencimentoRepositoryEloquent;

use App\Interfaces\PermissionRepository;
use App\Repositories\PermissionRepositoryEloquent;
use App\Interfaces\PermissionRoleRepository;
use App\Repositories\PermissionRoleRepositoryEloquent;
use App\Interfaces\RolesRepository;
use App\Repositories\RolesRepositoryEloquent;
use App\Interfaces\RoleUserRepository;
use App\Repositories\RoleUserRepositoryEloquent;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ArquivoCancelamentoRepository::class, ArquivoCancelamentoRepositoryEloquent::class);
        $this->app->bind(CancelamentoRepository::class, CancelamentoRepositoryEloquent::class);
        $this->app->bind(CancelamentoUsuarioRepository::class, CancelamentoUsuarioRepositoryEloquent::class);
        $this->app->bind(ClienteRepository::class, ClienteRepositoryEloquent::class);
        $this->app->bind(InteracoesCancelamentoRepository::class, InteracoesCancelamentoRepositoryEloquent::class);
        $this->app->bind(InteracoesUsuarioRepository::class, InteracoesUsuarioRepositoryEloquent::class);
        $this->app->bind(MotivoCancelamentoRepository::class, MotivoCancelamentoRepositoryEloquent::class);
        $this->app->bind(RecebimentoCartaRepository::class, RecebimentoCartaRepositoryEloquent::class);
        $this->app->bind(StatusCancelamentoRepository::class, StatusCancelamentoRepositoryEloquent::class);   
        $this->app->bind(AcessoUsuarioRepository::class, AcessoUsuarioRepositoryEloquent::class);
        $this->app->bind(ProdutoRepository::class, ProdutoRepositoryEloquent::class);
        $this->app->bind(BuscaVencimentoRepository::class, BuscaVencimentoRepositoryEloquent::class);
        $this->app->bind(PermissionRepository::class, PermissionRepositoryEloquent::class);
        $this->app->bind(PermissionRoleRepository::class, PermissionRoleRepositoryEloquent::class);
        $this->app->bind(RolesRepository::class, RolesRepositoryEloquent::class);
        $this->app->bind(RoleUserRepository::class, RoleUserRepositoryEloquent::class);
        
    }
}
