<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Interfaces\CancelamentoRepository;
use App\Models\User;

class CancelamentoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
