<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProdutoToCancelamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cancelamentos', function (Blueprint $table) {
            $table->unsignedBigInteger('id_produto')->after('id_recebimento_carta');

            $table->foreign('id_produto')->references('id')->on('produtos')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cancelamentos', function (Blueprint $table) {
            $table->dropColumn('id_produto');
        });
    }
}
