<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusTableArquivoCancelamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arquivo_cancelamentos', function (Blueprint $table) {
            $table->enum('status',['A','D'])->comment('A - Ativado','D - Desativado')->default('A')->after('observacoes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arquivo_cancelamentos', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
