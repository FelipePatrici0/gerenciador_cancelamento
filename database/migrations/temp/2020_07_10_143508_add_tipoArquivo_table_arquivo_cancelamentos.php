<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoArquivoTableArquivoCancelamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arquivo_cancelamentos', function (Blueprint $table) {
            $table->enum('tipo_arquivo',['Carta','OS'])->after('id_cancelamento');
            $table->renameColumn('carta', 'diretorio_arquivo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arquivo_cancelamentos', function (Blueprint $table) {
            $table->dropColumn('tipo_arquivo');
            $table->renameColumn('diretorio_arquivo', 'carta');
        });
    }
}
