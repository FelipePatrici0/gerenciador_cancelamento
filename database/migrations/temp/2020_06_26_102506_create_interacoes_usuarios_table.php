<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateInteracoesUsuariosTable.
 */
class CreateInteracoesUsuariosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('interacoes_usuarios', function(Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('id_interacoes_cancelamento');
			$table->integer('id_usuario');
			$table->enum('status',['A','D'])->comment('A - Ativado','D - Desativado')->default('A');
			$table->timestamps();
			
			$table->foreign('id_interacoes_cancelamento')->references('id')->on('interacoes_cancelamentos')->onDelete('RESTRICT');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('interacoes_usuarios');
	}
}
