<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCancelamentoUsuariosTable.
 */
class CreateCancelamentoUsuariosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cancelamento_usuarios', function(Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('id_cancelamento');
			$table->integer('id_usuario');
			$table->enum('status',['A','D'])->comment('A - Ativado','D - Desativado')->default('A');
			$table->timestamps();
			
			$table->foreign('id_cancelamento')->references('id')->on('cancelamentos')->onDelete('CASCADE');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cancelamento_usuarios');
	}
}
