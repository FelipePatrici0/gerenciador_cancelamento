<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRecebimentoCartasTable.
 */
class CreateRecebimentoCartasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recebimento_cartas', function(Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('descricao');
			$table->enum('status',['A','D'])->comment('A - Ativado','D - Desativado')->default('A');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recebimento_cartas');
	}
}
