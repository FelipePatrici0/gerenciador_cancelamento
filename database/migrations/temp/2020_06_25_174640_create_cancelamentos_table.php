<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCancelamentosTable.
 */
class CreateCancelamentosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('cancelamentos')) {

			Schema::create('cancelamentos', function(Blueprint $table) {
				$table->bigIncrements('id');
				$table->unsignedBigInteger('id_status_cancelamento');
				$table->unsignedBigInteger('id_motivo_cancelamento')->nullable();
				$table->unsignedBigInteger('id_recebimento_carta')->nullable();
				$table->string('nome');
				$table->integer('csid');
				$table->string('placa_veiculo')->nullable();
				$table->date('inicio_contrato')->nullable();
				$table->date('data_solicitacao')->nullable();
				$table->string('contato', 50)->nullable();
				$table->text('observacoes')->nullable();
				$table->enum('status',['A','D'])->comment('A - Ativado','D - Desativado')->default('A');
				$table->timestamps();
				
				$table->foreign('id_status_cancelamento')->references('id')->on('status_cancelamentos')->onDelete('RESTRICT');
				$table->foreign('id_motivo_cancelamento')->references('id')->on('motivo_cancelamentos')->onDelete('RESTRICT');
				$table->foreign('id_recebimento_carta')->references('id')->on('recebimento_cartas')->onDelete('RESTRICT');


			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cancelamentos');
	}
}
