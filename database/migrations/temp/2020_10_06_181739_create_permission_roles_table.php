<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePermissionRolesTable.
 */
class CreatePermissionRolesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permission_roles', function(Blueprint $table) {
            $table->increments('id');
			$table->integer('id_permission')->unsigned();
			$table->integer('id_role')->unsigned();

			$table->foreign('id_permission')->references('id')->on('permissions')->onDelete('cascade');
			$table->foreign('id_role')->references('id')->on('roles')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permission_roles');
	}
}
