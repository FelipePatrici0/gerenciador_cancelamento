<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateInteracoesCancelamentosTable.
 */
class CreateInteracoesCancelamentosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('interacoes_cancelamentos', function(Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('id_status_cancelamento');
			$table->unsignedBigInteger('id_cancelamento');
			$table->text('observacoes')->nullable();
			$table->timestamps();
			
			$table->foreign('id_status_cancelamento')->references('id')->on('status_cancelamentos')->onDelete('RESTRICT');
			$table->foreign('id_cancelamento')->references('id')->on('cancelamentos')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('interacoes_cancelamentos');
	}
}
