<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObservacoesTableArquivoCancelamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arquivo_cancelamentos', function (Blueprint $table) {
            $table->text('observacoes')->nullable()->after('ordem_servico');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arquivo_cancelamentos', function (Blueprint $table) {
            $table->dropColumn('observacoes');
        });
    }
}
