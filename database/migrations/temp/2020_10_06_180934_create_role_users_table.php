<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRoleUsersTable.
 */
class CreateRoleUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('role_users', function(Blueprint $table) {
            $table->increments('id');
			$table->bigInteger('id_user')->unsigned();
			$table->integer('id_role')->unsigned();		  

			$table->foreign('id_user')->references('id')->on('users')->onDelete('cascade'); 
			$table->foreign('id_role')->references('id')->on('roles')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('role_users');
	}
}
