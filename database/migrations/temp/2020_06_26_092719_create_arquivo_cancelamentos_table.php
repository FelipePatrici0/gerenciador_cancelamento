<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateArquivoCancelamentosTable.
 */
class CreateArquivoCancelamentosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('arquivo_cancelamentos', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('id_cancelamento');
			$table->string('carta')->unique()->nullable();
			$table->string('ordem_servico')->unique()->nullable();
			$table->timestamps();
			
			$table->foreign('id_cancelamento')->references('id')->on('cancelamentos')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('arquivo_cancelamentos');
	}
}
