<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterarAtributocancelamentoUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cancelamento_usuarios', function (Blueprint $table) {
            $table->unsignedBigInteger('id_usuario')->after('id_cancelamento');
           
            $table->foreign('id_usuario')->references('id')->on('users')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cancelamento_usuarios', function (Blueprint $table) {
            //
        });
    }
}
