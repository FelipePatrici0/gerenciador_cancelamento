<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateStatusCancelamentosTable.
 */
class CreateStatusCancelamentosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('status_cancelamentos', function(Blueprint $table) {
            $table->increments('id');
			$table->string('nome');
			$table->enum('status',['A','D'])->comment('A - Ativado','D - Desativado');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('status_cancelamentos');
	}
}
