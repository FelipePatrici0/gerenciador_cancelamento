<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoLigacaoTableCancelamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cancelamentos', function (Blueprint $table) {
            $table->enum('tipo_ligacao',['Receptiva','Ativa'])
                  ->after('observacoes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cancelamentos', function (Blueprint $table) {
            $table->dropColumn('tipo_ligacao');
        });
    }
}
