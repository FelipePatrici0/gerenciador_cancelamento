<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateStatusCancelamentosTable.
 */
class CreateStatusCancelamentosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('status_cancelamentos', function(Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('descricao');
			$table->enum('status',['A','D'])->comment('A - Ativado','D - Desativado')->default('A');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('status_cancelamentos');
	}
}
