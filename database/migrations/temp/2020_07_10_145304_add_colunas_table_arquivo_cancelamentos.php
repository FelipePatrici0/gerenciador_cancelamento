<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColunasTableArquivoCancelamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arquivo_cancelamentos', function (Blueprint $table) {
            $table->enum('tipo_arquivo',['Carta','Os'])->after('id_cancelamento');
            $table->string('diretorio_arquivo')->after('tipo_arquivo');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arquivo_cancelamentos', function (Blueprint $table) {
            $table->dropColumn('tipo_arquivo');
            $table->dropColumn('diretorio_arquivo');
        });
    }
}
